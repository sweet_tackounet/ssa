#! /bin/bash

IFS=$'\n'
player=''
fs=''
sm=''
key=''
q=''

mkdir test/.tmp_folder

mpv --help 2>&1 > /dev/null
if [ $? = 0 ]
then
    player='mpv'
    fs='--fs'
else
    vlc --help 2>&1 > /dev/null
    if [ $? = 0 ]
    then
        player='vlc'
        fs='-f'
        sm='--play-and-exit'
        key='--global-key-quit'
        q='q'
    else
        gnome-mplayer --help 2>&1 > /dev/null
        if [ $? = 0 ]
        then
            player='gnome-mplayer'
            fs='--fullscreen'
        else
            kmplayer --help 2>&1 > /dev/null
            if [ $? = 0 ]
            then
                player='kmplayer'
            else
                smplayer --help 2>&1 > /dev/null
                if [ $? = 0 ]
                then
                    player='smplayer'
                    fs='-fullscreen'
                    sm='-close-at-end'
                else
                    mplayer -v 2>&1 > /dev/null
                    if [ $? = 0 ]
                    then
                        player='mplayer'
                        fs='-fs'
                    else
                        echo 'no player are founded'
                        player="no-player"
                    fi
                fi
            fi
        fi
    fi
fi



for inode in $(ls test/videos/ | grep .ass)
do
    mv "test/videos/${inode}" test/.tmp_folder/
done

if [ -z $1 ]
then
    for inode in $(ls test/lyrics/ | grep .txt)
    do
        echo "============"
        echo "convert to ass => $inode"
        ./convert2ass --compute=test/videos --dst test/videos "test/lyrics/$inode"
        diff "test/ass/${inode%.txt}.ass" "test/videos/${inode%.txt}.ass" > /dev/null
        if [ $? = 0 ]
        then
            echo -e "\\033[5;32mPASS\\033[0m"
        else
            echo -e "\\033[5;31mFAIL\\033[0m"
            echo "----------------------------------"
        fi
        rm -f "test/videos/${inode%.txt}.ass" "test/videos/${inode%.txt}.frm" "test/videos/${inode%.txt}.lyr"
    done

elif [ $1 = "play" ]
then
    for inode in $(ls test/lyrics/ | grep .txt)
    do
        echo "============"
        echo "convert to ass => $inode"
        ./convert2ass --compute=test/videos --dst test/videos "test/lyrics/$inode"
        diff "test/ass/${inode%.txt}.ass" "test/videos/${inode%.txt}.ass" > /dev/null
        if [ $? = 0 ]
        then
            echo -e "\\033[5;32mPASS\\033[0m"
        else
            echo -e "\\033[5;31mFAIL\\033[0m"
            echo "----------------------------------"
        fi
    done
    if [ $player != "no-player" ]
    then
        $player test/videos/*.avi $fs $sm $key $q
    fi
    for inode in $(ls test/videos/ | grep -E "\\.(ass|lyr|frm)")
    do
        rm "test/videos/${inode}"
    done
fi

for inode in $(ls test/.tmp_folder/)
do
    mv "test/.tmp_folder/${inode}" test/videos/
done

rm -r test/.tmp_folder
