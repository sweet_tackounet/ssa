#ifndef MODIFIERS_H_
# define MODIFIERS_H_

# include "../convert/main.h"

unsigned int
get_colors(char* text, t_timing_line* t, unsigned int pos);
unsigned int
get_size(char* text, t_timing_line* t, unsigned int pos);
unsigned int
get_position(char* text, t_timing_line* t, unsigned int pos);

#endif /* MODIFIERS_H_ */
