#include "modifiers.h"

inline unsigned int
get_colors(char* text, t_timing_line* t, unsigned int pos)
{
  unsigned int ret = pos + 4;
  char color[9];

  for (unsigned int j = 0; 1; ++ret, ++j)
  {
    if (text[ret] == ':' || text[ret] == '$' || text[ret] == '}')
    {
      color[j] = 0;
      if (color[0] != 0)
      {
        t->color.flag += 1;
        if (t->color.flag == 1)
        {
          t->color.first = strtoul(color, NULL, 16);
          if (j == 6)
            t->color.first += 0xFF000000;
        }
        else
        {
          t->color.last = strtoul(color, NULL, 16);
          if (j == 6)
            t->color.last += 0xFF000000;
        }
      }
      if (text[ret] == '}')
        break;
      j = -1;
    }
    else
      color[j] = text[ret];
  }

  return ret;
}

inline unsigned int
get_size(char* text, t_timing_line* t, unsigned int pos)
{
  unsigned int ret = pos + 3;
  char size[5];

  for (unsigned int j = 0; text[ret] != 0; ++ret, ++j)
  {
    if (text[ret] == ':' || text[ret] == '}')
    {
      size[j] = 0;
      j = -1;
      t->size.flag += 1;
      if (t->size.flag == 1)
        t->size.first = strtoul(size, NULL, 10);
      else
        t->size.last = strtoul(size, NULL, 10);
      if (text[ret] == '}')
        break;
      continue;
    }
    size[j] = text[ret];
  }

  return ret;
}

inline unsigned int
get_position(char* text, t_timing_line* t, unsigned int pos)
{
  unsigned int ret = pos + 3;
  char position[5];

  for (unsigned int j = 0; text[ret] != 0; ++ret, ++j)
  {
    if (text[ret] == ',' || text[ret] == ':' || text[ret] == '}')
    {
      position[j] = 0;
      j = -1;
      if (text[ret] == ',')
      {
        t->position.flag += 1;
        if (t->position.flag == 1)
          t->position.first.x = strtol(position, NULL, 10);
        else
          t->position.last.x = strtol(position, NULL, 10);
      }
      else if (t->position.flag == 1)
        t->position.first.y = strtol(position, NULL, 10);
      else
        t->position.last.y = strtol(position, NULL, 10);
      if (text[ret] == '}')
        break;
    }
    else
      position[j] = text[ret];
  }

  return ret;
}
