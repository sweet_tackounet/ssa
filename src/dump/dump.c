#include "dump.h"

void
dump_lyrics(t_convert_ass* ass)
{
  unsigned int index = ass->i_lyrics;
  for (unsigned int i = 0; i < index; ++i)
    printf("%u. %s\n", i, ass->lyrics[i].text);
}

void
dump_time(t_convert_ass* ass)
{
  unsigned int index = ass->i_time;
  for (unsigned int i = 0; i < index; ++i)
    printf("%u. {%u}{%u}[%u]: %u\n", i, ass->time[i].start, ass->time[i].stop, ass->time[i].line, ass->time[i].length);
}

void
dump_all_lyrics_effect(t_convert_ass* ass)
{
  unsigned int index = ass->i_lyrics;
  for (unsigned int i = 0; i < index; ++i)
  {
    printf("\n%u. {%u}{%u}[%u]%s\n", i, ass->lyrics[i].start, ass->lyrics[i].stop, ass->lyrics[i].line, ass->lyrics[i].text);
    unsigned int length = ass->lyrics[i].length;
    unsigned int s = 0;
    for (unsigned int j = 0; j < length; ++j)
    {
      for (unsigned int k = s; k < MAX_EFFECT && ass->lyrics[i].character[j][k].free != 0; ++k)
      {
        if (j == 0
            && k == 0
            && (ass->lyrics[i].character[0][0].free == 2
                || ass->lyrics[i].character[0][0].free == 3))
          printf("extended\n");
        if (ass->lyrics[i].character[j][k].position.flag == 4)
        {
          s = 1;
          printf("[0] => {%u}{%u}",
                 ass->lyrics[i].character[j][k].start,
                 ass->lyrics[i].character[j][k].stop);
          if (ass->lyrics[i].character[j][k].color.flag == 1)
            printf("{c:$%.8lX}",
                   ass->lyrics[i].character[j][k].color.first);
          else if (ass->lyrics[i].character[j][k].color.flag == 2)
            printf("{c:$%.8lX:$%.8lX}",
                   ass->lyrics[i].character[j][k].color.first,
                   ass->lyrics[i].character[j][k].color.last);
          printf("{o:%i,%i:%i,%i}",
                 ass->lyrics[i].character[j][k].position.first.x,
                 ass->lyrics[i].character[j][k].position.first.y,
                 ass->lyrics[i].character[j][k].position.last.x,
                 ass->lyrics[i].character[j][k].position.last.y);
          if (ass->lyrics[i].character[j][k].size.flag == 1)
            printf("{s:%u}",
                   ass->lyrics[i].character[j][k].size.first);
          else if (ass->lyrics[i].character[j][k].size.flag == 2)
            printf("{s:%u:%u}",
                   ass->lyrics[i].character[j][k].size.first,
                   ass->lyrics[i].character[j][k].size.last);
          printf("%s", ass->lyrics[i].text);
        }
        else if (ass->lyrics[i].character[j][k].free != 3)
        {
          printf("%c: {%u}{%u}",
                 ass->lyrics[i].text[j],
                 ass->lyrics[i].character[j][k].start,
                 ass->lyrics[i].character[j][k].stop);
          if (ass->lyrics[i].character[j][k].color.flag == 1)
            printf("{c:$%.8lX}",
                   ass->lyrics[i].character[j][k].color.first);
          else if (ass->lyrics[i].character[j][k].color.flag == 2)
            printf("{c:$%.8lX:$%.8lX}",
                   ass->lyrics[i].character[j][k].color.first,
                   ass->lyrics[i].character[j][k].color.last);
          if (ass->lyrics[i].character[j][k].position.flag == 1)
            printf("{o:%i,%i}",
                   ass->lyrics[i].character[j][k].position.first.x,
                   ass->lyrics[i].character[j][k].position.first.y);
          else if (ass->lyrics[i].character[j][k].position.flag == 2)
            printf("{o:%i,%i:%i,%i}",
                   ass->lyrics[i].character[j][k].position.first.x,
                   ass->lyrics[i].character[j][k].position.first.y,
                   ass->lyrics[i].character[j][k].position.last.x,
                   ass->lyrics[i].character[j][k].position.last.y);
          if (ass->lyrics[i].character[j][k].size.flag == 1)
            printf("{s:%u}",
                   ass->lyrics[i].character[j][k].size.first);
          else if (ass->lyrics[i].character[j][k].size.flag == 2)
            printf("{s:%u:%u}",
                   ass->lyrics[i].character[j][k].size.first,
                   ass->lyrics[i].character[j][k].size.last);
        }
        printf("\n");
      }
      printf("\n");
    }
  }
}

void
dump_all_lyrics_free(t_convert_ass ass)
{
  unsigned int index = ass.i_lyrics;
  for (unsigned int i = 0; i < index; ++i)
    printf("%u. {%u}{%u}[%u]%s<%u>\n", i, ass.lyrics[i].start, ass.lyrics[i].stop, ass.lyrics[i].line, ass.lyrics[i].text, ass.lyrics[i].character[0][0].free);
}

void
dump_t_lyrics(t_lyrics l)
{
  printf("{%u}{%u}[%u]%s\n", l.start, l.stop, l.line, l.text);
  unsigned int length = l.length;
  for (unsigned int j = 0; j < length; ++j)
  {
    for (unsigned int k = 0; k < 4; ++k)
    {
      printf("%c: {%u}{%u}",
             l.text[j],
             l.character[j][k].start,
             l.character[j][k].stop);
      if (l.character[j][k].color.flag == 1)
        printf("{c:$%.8lX}",
               l.character[j][k].color.first);
      else if (l.character[j][k].color.flag == 2)
        printf("{c:$%.8lX:$%.8lX}",
               l.character[j][k].color.first,
               l.character[j][k].color.last);
        if (l.character[j][k].position.flag == 1)
          printf("{o:%i,%i}",
                 l.character[j][k].position.first.x,
                 l.character[j][k].position.first.y);
        else if (l.character[j][k].position.flag == 2)
          printf("{o:%i,%i:%i,%i}",
                 l.character[j][k].position.first.x,
                 l.character[j][k].position.first.y,
                 l.character[j][k].position.last.x,
                 l.character[j][k].position.last.y);
        if (l.character[j][k].size.flag == 1)
          printf("{s:%u}",
                 l.character[j][k].size.first);
        else if (l.character[j][k].size.flag == 2)
          printf("{s:%u:%u}",
                 l.character[j][k].size.first,
                 l.character[j][k].size.last);
        printf("\n");
    }
    printf("\n");
  }
}

void
dump_all_lyrics_total_effect(t_extended_lyrics* el, char* text, unsigned int length)
{
  for (unsigned int j = 0; j < length; ++j)
  {
    for (unsigned int k = 0; k < MAX_TOTAL_EFFECT && el->character[j][k].free != 0 && el->character[j][k].free != 3; ++k)
    {
      printf("%c: {%u}{%u}",
             text[j],
             el->character[j][k].start,
             el->character[j][k].stop);
      if (el->character[j][k].color.flag == 1)
        printf("{c:$%.8lX}",
               el->character[j][k].color.first);
      else if (el->character[j][k].color.flag == 2)
        printf("{c:$%.8lX:$%.8lX}",
               el->character[j][k].color.first,
               el->character[j][k].color.last);
      if (el->character[j][k].position.flag == 1)
        printf("{o:%i,%i}",
               el->character[j][k].position.first.x,
               el->character[j][k].position.first.y);
      else if (el->character[j][k].position.flag == 2)
        printf("{o:%i,%i:%i,%i}",
               el->character[j][k].position.first.x,
               el->character[j][k].position.first.y,
               el->character[j][k].position.last.x,
               el->character[j][k].position.last.y);
      if (el->character[j][k].size.flag == 1)
        printf("{s:%u}",
               el->character[j][k].size.first);
      else if (el->character[j][k].size.flag == 2)
        printf("{s:%u:%u}",
               el->character[j][k].size.first,
               el->character[j][k].size.last);
      printf("\n");
    }
    printf("\n");
  }
}

void
dump_lyrics_cache(t_lyrics* lyrics)
{
  for (unsigned int i = 0; i < BUFFER_SIZE; ++i)
    printf("%u. %s\n", i, lyrics[i].text);
}

void
dump(t_timing_line t)
{
  printf("{%u}{%u}", t.start, t.stop);
  for (unsigned int j = 0; j < t.line; ++j)
    printf("|");
  if (t.color.flag == 1)
    printf("{c:$%.8lX}", t.color.first);
  else if (t.color.flag == 2)
    printf("{c:$%.8lX:$%.8lX}", t.color.first, t.color.last);
  if (t.position.flag == 1)
    printf("{o:%i,%i}", t.position.first.x, t.position.first.y);
  else if (t.position.flag == 2)
    printf("{o:%i,%i:%i,%i}", t.position.first.x, t.position.first.y, t.position.last.x, t.position.last.y);
  if (t.size.flag == 1)
    printf("{s:%u}", t.size.first);
  else if (t.size.flag == 2)
    printf("{s:%u:%u}", t.size.first, t.size.last);
  printf("%s\n", t.text);
}

void
dump_all(t_timing_line* t, unsigned int index)
{
  for (unsigned int i = 0; i < index; ++i)
    dump(t[i]);
}
