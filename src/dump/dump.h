#ifndef DUMP_H_
# define DUMP_H_

# include "../convert/handle.h"

void
dump_lyrics(t_convert_ass* ass);
void
dump_time(t_convert_ass* ass);
void
dump_all_lyrics_effect(t_convert_ass* ass);
void
dump_t_lyrics(t_lyrics l);
void
dump_lyrics_cache(t_lyrics* lyrics);
void
dump(t_timing_line t);
void
dump_all(t_timing_line* t, unsigned int index);
void
dump_all_lyrics_free(t_convert_ass ass);
void
dump_all_lyrics_total_effect(t_extended_lyrics* el, char* text, unsigned int length);

#endif /* !DUMP_H_ */
