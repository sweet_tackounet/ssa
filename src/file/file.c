#include "file.h"

void
replace(char* str, unsigned int index, const char* add)
{
  unsigned int i = index;
  for (unsigned int j = 0; add[j] != 0; ++i, ++j)
    str[i] = add[j];
  str[i] = 0;
}

int
is_txt(const char* str, unsigned int n)
{
  if (str[n] == '.'
      && str[n + 1] == 't'
      && str[n + 2] == 'x'
      && str[n + 3] == 't')
    return 1;
  return 0;
}

char*
get_filename(const char* file)
{
  unsigned int n = strlen(file);
  char* filename = calloc(n + 1, sizeof (char));
  int d = 0; 
  int e = n;

  if (filename == NULL)
  {
    fprintf(stderr, "Cannot calloc filename: %s\n", file);
    return NULL;
  }

  for (unsigned int i = 0; i < n; ++i)
    if (file[i] == '/')
      d = i;
    else if (file[i] == '.')
      e = i;

  strncpy(filename, file + d + 1, e - d - 1);
  return filename;
}

char*
get_default_folder(const char* file)
{
  unsigned int n = strlen(file);
  char* path = calloc(n + 1, sizeof (char));
  int d = 0; 

  if (path == NULL)
  {
    fprintf(stderr, "Cannot calloc default folder: %s\n", file);
    return NULL;
  }

  for (unsigned int i = 0; i < n; ++i)
    if (file[i] == '/')
      d = i + 1;

  if (d == 0)
    return NULL;

  strncpy(path, file, d);
  return path;
}

FILE*
get_path_file(const char* file, const char* folder, const char* ext, const char* mode)
{
  char* filename = get_filename(file);

  if (filename == NULL)
    return NULL;

  char* path = NULL;
  int n = 0;
  if (folder == NULL)
  {
    path = get_default_folder(file);
    if (path == NULL)
    {
      free(filename);
      return NULL;
    }
  }
  else
  {
    n = strlen(folder);
    path = calloc(n + 2, sizeof (char));
    strcpy(path, folder);
    path[n] = '/';
    n += 1;
  }

  n += strlen(file) + 5;
  char* name_path_file = calloc(n, sizeof (char));
  strcpy(name_path_file, path);
  strcat(name_path_file, filename);
  strcat(name_path_file, ext);
  free_2(path, filename);

  FILE* path_file = fopen(name_path_file, mode);
  free(name_path_file);

  return path_file;
}