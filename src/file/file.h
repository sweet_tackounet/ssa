#ifndef FILE_H_
# define FILE_H_

# include <string.h>
# include "../convert/handle.h"

FILE*
get_path_file(const char* file, const char* folder, const char* ext, const char* mode);
char*
get_filename(const char* file);

#endif /* FILE_H_ */
