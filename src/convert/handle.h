#ifndef HANDLE_H_
# define HANDLE_H_

# define _GNU_SOURCE
# define MAX_LINE_TIMING 16384
# define MAX_TIME 1024
# define MAX_LYRICS 128
# define SIZE_TEXT 128
# define MAX_LEN 64
# define MAX_EFFECT 16
# define EXTEND_EFFECT 64
# define MAX_TOTAL_EFFECT 1024 // = MAX_EFFECT x EXTEND_EFFECT
# define BUFFER_SIZE 8
# define CACHE_SIZE 16

# define ALPHA(X) ((int) ((~(X) >> 24) & 0xFF))
# define COLOR(X) ((X) & 0xFFFFFF)

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <getopt.h>
# include <stdarg.h>

typedef unsigned long t_color;

typedef struct
{
  unsigned int flag;
  t_color first;
  t_color last;
} t_color_handle;

typedef struct
{
  int x;
  int y;
} t_position;

typedef struct
{
  unsigned int flag;
  unsigned int rx;
  unsigned int ry;
  t_position first;
  t_position last;
} t_position_handle;

typedef struct
{
  unsigned int flag;
  unsigned int first;
  unsigned int last;
} t_size_handle;

typedef struct
{
  unsigned int start;
  unsigned int stop;
  unsigned int line;
  t_color_handle color;
  t_position_handle position;
  t_size_handle size;
  char text[MAX_LEN];
} t_timing_line;

typedef struct
{
  int flag;
  int fps;
  int ratio;
} t_compute_handle;

typedef struct
{
  float fps;
  float font;
  float angle_logo;
  float max_angle;
  float move_angle;
  unsigned int logo_size;
  unsigned int margin_sub;
  unsigned int margin_logo;
  unsigned int height_line;
  unsigned int credit_y;
  float move_x;
  float move_y;  
} t_handle;

typedef struct
{
  unsigned int start;
  unsigned int stop;
  unsigned int line;
  t_position_handle position;
  unsigned int characters;
  unsigned int length;
  unsigned int hit;
} t_time;

typedef struct
{
  unsigned int free;
  unsigned int start;
  unsigned int stop;
  t_color_handle color;
  t_size_handle size;
  t_position_handle position;
} t_character_effect;

typedef struct
{
  unsigned int start;
  unsigned int stop;
  unsigned int length;
  unsigned int line;
  char text[MAX_LEN];
  t_character_effect character[MAX_LEN][MAX_EFFECT];
} t_lyrics;

typedef struct
{
  unsigned int start;
  unsigned int stop;
  t_character_effect** character;
} t_extended_lyrics;

typedef struct
{
  t_lyrics* lyrics;
  t_time* time;
  unsigned int i_lyrics;
  unsigned int i_time;
} t_convert_ass;

typedef struct
{
  unsigned int start;
  t_color color;
  int x_pos;
  int y_pos;
  unsigned int pos;
} t_previous_data;

typedef struct
{
  int save;
  char* folder;
  char* dst;
} t_name_handle;

void
set_handle(const char* opt, t_handle* handle);

/*
void
free_all(void* ptr, ...);
*/

void
free_2(void* ptr1, void* ptr2);
/*
void
free_3(void** ptr1, void** ptr2, void** ptr3);
void
free_4(void** ptr1, void** pt2, void** ptr3, void** ptr4);
*/

t_timing_line
init_timing_line();
void
dump(t_timing_line t);
void
dump_all(t_timing_line* t, unsigned int index);
// void
// print_handle(t_handle handle);

#endif /* HANDLE_H_ */
