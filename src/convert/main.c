#include "main.h"

static int
init_convert_ass(t_convert_ass* ass)
{
  ass->lyrics = calloc(MAX_LYRICS, sizeof (t_lyrics));
  if (ass->lyrics == NULL)
  {
    fprintf(stderr, "Cannot allocate lyrics buffer\n");
    return 0;
  }
  ass->time = calloc(MAX_TIME, sizeof (t_time));
  if (ass->time == NULL)
  {
    fprintf(stderr, "Cannot allocate time buffer\n");
    free(ass->lyrics);
    return 0;
  }
  ass->i_lyrics = 0;
  ass->i_time = 0;
  return 1;
}

static void
get_info_video(const char* file, t_handle* handle, const char* folder)
{
  FILE* video_file = get_path_file(file, folder, ".avi", "rb");
  if (video_file == NULL)
  {
    fprintf(stderr, "cannot open video file: %s\n", file);
    return;
  }

  int delay = 0;
  int width = 0;
  int height = 0;

  fseek(video_file, sizeof (char) * 32, SEEK_SET);
  if (fread(&delay, sizeof (char) * 4, 1, video_file) != 1)
  {
    fprintf(stderr, "cannot read time delay between frames: %s\n", file);
    fclose(video_file);
    return;
  }
  fseek(video_file, sizeof (char) * 64, SEEK_SET);
  if (fread(&width, sizeof (char) * 4, 1, video_file) != 1)
  {
    fprintf(stderr, "cannot read width video image: %s\n", file);
    fclose(video_file);
    return;
  }
  fseek(video_file, sizeof (char) * 68, SEEK_SET);
  if (fread(&height, sizeof (char) * 4, 1, video_file) != 1)
  {
    fprintf(stderr, "cannot read height video image: %s\n", file);
    fclose(video_file);
    return;
  }

  handle->fps = 1000000. / (float) delay;
  if (!(((float) (width * 9) / (float) (height * 16)) < 1.01
           && ((float) (width * 9) / (float) (height * 16)) > 0.99))
  {
    if (((float) (width * 3) / (float) (height * 4)) < 1.01
        && ((float) (width * 3) / (float) (height * 4)) > 0.99)
      set_handle("4:3", handle);
    else
      fprintf(stderr, "unknown aspect ratio: %ix%i\n", width, height);
  }

  fclose(video_file);
}

static void
process(const char* file, t_handle handle, t_name_handle name, t_compute_handle compute)
{
  t_timing_line* timing_line = NULL;
  unsigned int index = 0;
  t_convert_ass ass;

  if (compute.flag == 1 && (compute.fps == 1 || compute.ratio == 1))
    get_info_video(file, &handle, name.folder);

  timing_line = get_timing_lines(file, &index);
  if (timing_line == NULL)
  {
    fprintf(stderr, "Cannot get lines from %s\n", file);
    return;
  }

  if (init_convert_ass(&ass)
      && convert_to_ass(timing_line, index, &ass))
  {
    write_file(file, ass, handle, name);
    free_2(ass.lyrics, ass.time);
  }
  free(timing_line);
}

int
main(int argc, char* argv[])
{
  int c = 0;
  t_name_handle name =
  {
    .save = 1,
    .folder = NULL,
    .dst = NULL
  };
  t_compute_handle compute =
  {
    .flag = 0,
    .fps = 1,
    .ratio = 1
  };
  t_handle handle =
    {
      .fps = 25,
      .font = 24,
      .angle_logo = .413 / 98.,
      .max_angle = .195,
      .move_angle = 2.328494 / 1083.88,
      .logo_size = 80,
      .margin_sub = 10,
      .margin_logo = 8,
      .height_line = 30,
      .credit_y = 14,
      .move_x = 0.0088,
      .move_y = 0.0000553
    };

  do {
    static struct option long_options[] =
      {
        {"compute", optional_argument, NULL, 'c'},
        {"dst", required_argument, NULL, 'd'},
        {"fps", required_argument, NULL, 'f'},
        {"ratio", required_argument, NULL, 'r'},
        {"save", no_argument, NULL, 's'},
        {0, 0, 0, 0}
      };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "c::d:f:r:s", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c)
    {
      case 'c':
        name.folder = calloc(strlen(optarg) + 1, sizeof (char));
        strcpy(name.folder, optarg);
        compute.flag = 1;
        break;

      case 'd':
        name.dst = calloc(strlen(optarg) + 1, sizeof (char));
        strcpy(name.dst, optarg);
        break;

      case 'f':
        handle.fps = strtod(optarg, NULL);
        compute.fps = 0;
        break;

      case 'r':
        set_handle(optarg, &handle);
        compute.ratio = 0;
        break;

      case 's':
        name.save = 1;
        break;

      default:
        break;
    }
  } while (1);
  

  while (optind < argc)
    process(argv[optind++], handle, name, compute);

  free_2(name.folder, name.dst);
  return 0;
}
