#ifndef CONVERT_H_
# define CONVERT_H_

# include "main.h"
# include "../dump/dump.h"

int
convert_to_ass(t_timing_line* t,
               unsigned int size,
               t_convert_ass* ass);

#endif /* !CONVERT_H_ */
