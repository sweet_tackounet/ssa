#include "handle.h"

inline void
set_handle(const char* opt, t_handle* handle)
{
  if (!strcmp(opt, "4:3"))
  {
    handle->font = 21.5;
    handle->angle_logo = .3705 / 98.;
    handle->max_angle = .175;
    handle->move_angle = 2.088879 / 1294.58;
    handle->logo_size = 70;
    handle->margin_sub = 32;
    handle->margin_logo = 32;
    handle->height_line = 27;
    handle->credit_y = 15;
    handle->move_x = 0.008;
    handle->move_y = 0.00007575;
  }
}

/*
void
free_all(void* ptr, ...)
{
  va_list argp;
  void* p = NULL;

  va_start(argp, ptr);
  while ((p = va_arg(argp, void*)) != 0)
    free(p);
  va_end(argp);
}
*/

inline void
free_2(void* ptr1, void* ptr2)
{
  free(ptr1);
  free(ptr2);
}

/*
inline void
free_3(void** ptr1, void** ptr2, void** ptr3)
{
  free(*ptr1);
  free(*ptr2);
  free(*ptr3);
}

inline void
free_4(void** ptr1, void** ptr2, void** ptr3, void** ptr4)
{
  free(*ptr1);
  free(*ptr2);
  free(*ptr3);
  free(*ptr4);
}
*/

t_timing_line
init_timing_line()
{
  t_timing_line t;

  t.start = 0;
  t.stop = 0;
  t.line = 0;
  t.color.flag = 0;
  t.color.first = 0;
  t.color.last = 0;
  t.position.flag = 0;
  t.position.first.x = 0;
  t.position.first.y = 0;
  t.position.last.x = 0;
  t.position.last.y = 0;
  t.size.flag = 0;
  t.size.first = 0;
  t.size.last = 0;

  return t;
}

// void
// print_handle(t_handle handle)
// {
//   printf("fps:%.3f\nleft:%.3f\nspace:%.3f\nfont:%.3f\nlogo:%d\nmsub:%d\nmlog:%d\nheight:%d\ncredit:%d\n",
//          handle.fps, handle.left_position, handle.space, handle.font,
//          handle.logo_size, handle.margin_sub, handle.margin_logo,
//          handle.height_line, handle.credit_y);
// }
