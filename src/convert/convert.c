#include "convert.h"

static inline t_lyrics
init_lyrics()
{
  t_lyrics t =
    {
      .start = 0,
      .stop = 0,
      .length = 0,
      .line = 0
    };

  for (unsigned i = 0; i < MAX_LEN; ++i)
  {
    t.text[i] = 0;
    for (unsigned int j = 0; j < MAX_EFFECT; ++j)
    {
      t.character[i][j].free = 0;
      t.character[i][j].start = 0;
      t.character[i][j].stop = 0;
      t.character[i][j].color.flag = 0;
      t.character[i][j].color.first = 0;
      t.character[i][j].color.last = 0;
      t.character[i][j].size.flag = 0;
      t.character[i][j].size.first = 0;
      t.character[i][j].size.last = 0;
      t.character[i][j].position.flag = 0;
      t.character[i][j].position.first.x = 0;
      t.character[i][j].position.first.y = 0;
      t.character[i][j].position.last.x = 0;
      t.character[i][j].position.last.y = 0;
    }
  }

  return t;
}

static int
check_type(const char* text)
{
  for (int i = 0; text[i] != 0; ++i)
    if (text[i] != ' ')
      return (text[i] == (char) 255
              || (text[i] == (char) -61 && text[i + 1] == (char) -65));
  return 0;
}

static void
characters_copy(t_character_effect* dst, t_character_effect src)
{
  dst->free = src.free;
  dst->start = src.start;
  dst->stop = src.stop;
  dst->color.flag = src.color.flag;
  dst->color.first = src.color.first;
  dst->color.last = src.color.last;
  dst->size.flag = src.size.flag;
  dst->size.first = src.size.first;
  dst->size.last = src.size.last;
  dst->position.flag = src.position.flag;
  dst->position.first.x = src.position.first.x;
  dst->position.first.y = src.position.first.y;
  dst->position.last.x = src.position.last.x;
  dst->position.last.y = src.position.last.y;
}

static void
save_character(t_character_effect* c, t_timing_line t)
{
  c->free = 1;
  c->start = t.start;
  c->stop = t.stop;
  c->color.flag = t.color.flag;
  c->color.first = t.color.first;
  c->color.last = t.color.last;
  c->size.flag = t.size.flag;
  c->size.first = t.size.first;
  c->size.last = t.size.last;
  c->position.flag = t.position.flag;
  c->position.first.x = t.position.first.x;
  c->position.first.y = t.position.first.y;
  c->position.last.x = t.position.last.x;
  c->position.last.y = t.position.last.y;
}

static void
save_time(t_timing_line t, t_time* time, t_timing_line* cache, unsigned int c)
{
  time->start = t.start;
  time->stop = t.stop;
  time->line = t.line;
  time->position.flag = t.position.flag;
  time->position.first.x = t.position.first.x;
  time->position.first.y = t.position.first.y;
  time->position.last.x = t.position.last.x;
  time->position.last.y = t.position.last.y;

  for (int i = c, j = 0; j < CACHE_SIZE; i = (i - 1) & (CACHE_SIZE - 1), ++j)
  {
    unsigned int hit = 0;
    for (; t.text[hit] == ' '; ++hit);

    if (cache[i].start == t.start
        && (cache[i].stop == t.stop
            || ((t.position.flag & ~8) == 2
                && t.position.first.x == cache[i].position.first.x
                && t.position.first.y == cache[i].position.first.y))
        && cache[i].line == t.line - 1)
    {
      unsigned int hit_cache = 0;
      for (; cache[i].text[hit_cache] == ' '; ++hit_cache);
      if (hit == hit_cache)
      {
        unsigned int characters = 0;
        unsigned int length = hit;
        for (; cache[i].text[length]; ++length)
        {
          if (cache[i].text[length] != ' ')
            ++characters;
        }
        time->hit = hit;
        time->characters = characters;
        time->length = length;
        return;
      }
    }
  }

  fprintf(stderr, "Cannot find instant of time %u %u\n", t.start, t.stop);
}

static void
save_subtitle(t_lyrics* lyrics, t_lyrics l)
{
  unsigned int length = l.length;
  unsigned int s = 1;

  lyrics->start = l.start;
  lyrics->stop = l.stop;
  lyrics->length = length;
  lyrics->line = l.line;
  strcpy(lyrics->text, l.text);

  s = 0;
  for (unsigned int i = 0; i < length; ++i)
    for (unsigned int j = s; j < MAX_EFFECT && l.character[i][j].free != 0; ++j)
    {
      if (i == 0
          && j == 0
          && (l.character[0][0].position.flag & ~8) == 4)
        s = 1;
      characters_copy(&(lyrics->character[i][j]), l.character[i][j]);
    }
}

static inline void
free_effect(t_lyrics* bs)
{
  for (unsigned int i = 0; i < MAX_LEN; ++i)
    for (unsigned int j = 0; j < MAX_EFFECT; ++j)
    {
      bs->character[i][j].free = 0;
      bs->character[i][j].start = 0;
      bs->character[i][j].stop = 0;
      bs->character[i][j].color.flag = 0;
      bs->character[i][j].color.first = 0;
      bs->character[i][j].color.last = 0;
      bs->character[i][j].size.flag = 0;
      bs->character[i][j].size.first = 0;
      bs->character[i][j].size.last = 0;
      bs->character[i][j].position.flag = 0;
      bs->character[i][j].position.first.x = 0;
      bs->character[i][j].position.first.y = 0;
      bs->character[i][j].position.last.x = 0;
      bs->character[i][j].position.last.y = 0;
    }
}

static inline void
free_buffer_subtitle(t_lyrics* bs)
{
  for (int i = 0; i < MAX_LEN; ++i)
    bs->text[i] = 0;
  free_effect(bs);
}

static void
save_slider(t_lyrics* buffer, t_timing_line t)
{
  save_character(&(buffer->character[0][0]), t);
  strcpy(buffer->text, t.text);

  buffer->character[0][0].free = 2;
  buffer->character[0][0].position.flag = 3;
  buffer->character[0][0].position.last.x = t.position.first.x;
  buffer->character[0][0].position.last.y = 92;
}

static void
save_credit(t_lyrics* buffer, t_timing_line t)
{
  save_character(&(buffer->character[0][0]), t);
  strcpy(buffer->text, t.text);
  buffer->character[0][0].size.flag = 3;
}

static void
save_lyrics(t_lyrics* buffer, t_timing_line t)
{
  unsigned int length = strlen(t.text);

  buffer->start = t.start;
  buffer->stop = t.stop;
  buffer->length = length;
  buffer->line = t.line;

  // Slide effect
  if (t.stop - t.start == 1
      && (t.position.flag & 7) == 1
      && t.position.first.x > 1000
      && t.position.first.y == 0
      && t.color.flag == 1)
    save_slider(buffer, t);
  else if ((t.position.flag & 7) == 1
           && t.position.first.x == 20
           && t.position.first.y == 540
           && t.size.flag == 1
           && t.size.first == 25
           && t.text[0] == '[')
    save_credit(buffer, t);
  else
    for (unsigned int i = 0; i < length; ++i)
    {
      if (t.text[i] != ' ')
        save_character(&(buffer->character[i][0]), t);
      buffer->text[i] = t.text[i];
    }
  buffer->text[length] = 0;
}

static void
merge_slide_effect_init(t_lyrics* buffer, t_timing_line t)
{
  buffer->character[0][0].position.last.y -= 2;

  if (buffer->character[0][0].position.last.y < 7)
    buffer->character[0][0].position.flag = 5;

  buffer->character[0][0].stop = t.stop;
  buffer->character[0][0].position.last.x = t.position.first.x;
}

static void
merge_slide_effect_transition(t_lyrics* buffer, t_timing_line t)
{
  if (buffer->character[0][0].position.last.x - t.position.first.x < 4)
  {
    buffer->character[0][0].position.flag = 4;
    return;
  }

  buffer->character[0][0].stop = t.stop;
  buffer->character[0][0].position.last.x = t.position.first.x;
}

static void
merge_slide_effect_main(t_lyrics* buffer, t_timing_line t)
{
  if (t.stop > buffer->stop)
    buffer->stop = t.stop;

  if (buffer->character[0][1].free == 0)
  {
    buffer->character[0][0].stop = t.start - 1;
    buffer->character[0][1].free = 1;
    buffer->character[0][0].position.first.x = t.position.first.x + 3;
    buffer->character[0][0].position.first.y = 0;
    buffer->character[0][0].position.last.x = 0;
    buffer->character[0][0].position.last.y = 0;
  }

  if (ALPHA(t.color.first) == 0)
  {
    if (COLOR(t.color.first) != COLOR(buffer->character[0][0].color.first))
    {
      if (buffer->character[0][1].start == 0)
      {
        save_character(&(buffer->character[0][1]), t);
        buffer->character[0][1].stop = 0;
        buffer->character[0][1].color.flag = 2;
        buffer->character[0][1].position.flag = 0;
        buffer->character[0][1].position.first.x = 0;
        buffer->character[0][1].position.first.y = 0;
      }
      else if (COLOR(t.color.first) == COLOR(buffer->character[0][1].color.first))
      {
        unsigned int length = buffer->length;

        for (unsigned int i = 1; i < length; ++i)
          if (t.text[i] != ' ')
          {
            save_character(&(buffer->character[i][1]), t);
            buffer->character[i][1].stop = 0;
            buffer->character[i][1].color.flag = 2;
            buffer->character[i][1].position.flag = 0;
            buffer->character[i][1].position.first.x = 0;
            buffer->character[i][1].position.first.y = 0;
            break;
          }
      }
    }
  }
  else
  {
    if (!strcmp(t.text, buffer->text))
    {
      if (buffer->character[0][2].free == 0)
      {
        buffer->character[0][2].free = 1;
        buffer->character[0][2].start = t.start;
        buffer->character[0][0].position.last.x = t.position.first.x;

        unsigned int length = buffer->length;
        for (unsigned int i = 0; i < length; ++i)
          if (t.text[i] != ' '
              && buffer->character[i][1].free == 1
              && buffer->character[i][1].stop == 0)
          {
            buffer->character[i][1].stop = t.start;
            buffer->character[i][1].color.last = COLOR(buffer->character[0][1].color.last) | 0xFF000000;
            break;
          }
      }
    }
    else
    {
      unsigned int length = buffer->length;

      for (unsigned int i = 0; i < length; ++i)
        if (t.text[i] != ' ')
        {
          if (buffer->character[i][1].stop == 0)
          {
            buffer->character[i][1].stop = t.start - 1;
            buffer->character[i][1].color.last = COLOR(t.color.first) | 0xFF000000;
          }
          break;
        }
    }
  }
}

static void
merge_lyrics(t_lyrics* buffer, t_timing_line t, int* save)
{
  unsigned int length = buffer->length;

  if (buffer->stop < t.stop)
    buffer->stop = t.stop;
  if (buffer->start > t.start)
    buffer->start = t.start;

  // Slide effect
  if (t.stop - t.start == 1
      && (t.position.flag & 7) == 1
      && t.position.first.y == 0
      && t.color.flag == 1
      && (buffer->character[0][0].position.flag & 7) > 2)
    if ((buffer->character[0][0].position.flag & 7) == 3
        && (buffer->character[0][0].position.last.x - t.position.first.x) == buffer->character[0][0].position.last.y)
      merge_slide_effect_init(buffer, t);
    else if ((buffer->character[0][0].position.flag & 7) == 4)
      merge_slide_effect_main(buffer, t);
    else if ((buffer->character[0][0].position.flag & 7) == 5)
    {
      merge_slide_effect_transition(buffer, t);
      if ((buffer->character[0][0].position.flag & 7) == 4)
        merge_slide_effect_main(buffer, t);
    }
    else
    {
      printf("perdu %u [%s] %u\n", buffer->character[0][0].position.last.y, t.text, buffer->character[0][0].position.flag);
      dump(t);
    }
  else if (buffer->character[0][0].size.flag == 3)
  {
    unsigned int i = 1;
    for (; buffer->character[0][i].free != 0; ++i);
    save_character(&(buffer->character[0][i]), t);
  }
  else
    for (unsigned int i = 0; i < length; ++i)
    {
      if (t.text[i] != ' ')
      {
        for (unsigned int j = 0; j < MAX_EFFECT; ++j)
          if (buffer->character[i][j].free == 0)
          {
            if (j == 0
                || buffer->character[i][j - 1].color.flag != t.color.flag
                || buffer->character[i][j - 1].color.first != t.color.first
                || buffer->character[i][j - 1].color.last != t.color.last
                || buffer->character[i][j - 1].size.flag != t.size.flag
                || buffer->character[i][j - 1].size.first != t.size.first
                || buffer->character[i][j - 1].size.last != t.size.last
                || buffer->character[i][j - 1].position.flag != t.position.flag
                || buffer->character[i][j - 1].position.first.x != t.position.first.x
                || buffer->character[i][j - 1].position.first.y != t.position.first.y
                || buffer->character[i][j - 1].position.last.x != t.position.last.x
                || buffer->character[i][j - 1].position.last.y != t.position.last.y)
            {
              save_character(&(buffer->character[i][j]), t);
              if (j == (MAX_EFFECT - 1))
                *save = 1;
              if ((t.position.flag & ~8) != 0
                  && buffer->character[0][0].position.flag < 8)
                buffer->character[0][0].position.flag += 8;
            }
            else
              buffer->character[i][j - 1].stop = t.stop;
            break;
          }
        buffer->text[i] = t.text[i];
      }
    }
}

static inline int
deep_check(t_lyrics buffer, t_timing_line t)
{
  unsigned int length = buffer.length;
  unsigned int ret = 0;

  for (unsigned int i = 0; i < length; ++i)
    if (t.text[i] != ' ')
    {
      if (buffer.text[i] != ' ')
      {
        if (buffer.text[i] != t.text[i])
          return 0;
        else if (buffer.character[i][0].free != 0)
        {
          int j = 1;
          while (buffer.character[i][j].free != 0)
            ++j;
          if (buffer.character[i][j - 1].stop != t.start)
            return 0;
        }
        ret = 2;
      }
      else if (t.color.flag == 2
               && (t.color.last & 0xFF000000) == 0)
        return 0;
      else if (t.start == 0)
        return 1;
      else
      {
        for (unsigned int j = i + 1; j < length; ++j)
          if (buffer.character[j][0].free == 1)
            return 0;
        ret = 1;
      }
    }

  return ret;
}

static inline int
quick_check(t_lyrics buffer, t_timing_line t)
{
  return (buffer.text[0] != 0
          && buffer.length == strlen(t.text)
          && buffer.line == t.line
          && ((buffer.start <= t.start
               && buffer.stop >= t.start)
              || (buffer.start >= t.start
                  && buffer.start <= t.stop)));
}

static inline int
slide_check(t_lyrics buffer, t_timing_line t)
{
  unsigned int length = buffer.length;
  if (strlen(t.text) != length
      || t.line != buffer.line)
    return 0;

  if (t.stop - t.start == 1)
  {
    for (unsigned int i = 0; i < length; ++i)
      if (buffer.text[i] != ' '
          && t.text[i] != ' '
          && buffer.text[i] != t.text[i])
        return 0;
    return 1;
  }
  return 0;
}

static inline int
credit_check(t_lyrics buffer, t_timing_line t)
{
  if ((t.position.flag & ~8) == 1
      && t.position.first.x == 20
      && t.position.first.y == 540
      && t.size.flag == 1
      && t.size.first == 25
      && t.text[0] == '['
      && buffer.character[0][0].size.first == 3
      && buffer.text[0] == '[')
    return 1;
  return 0;

}

static unsigned int
find_index(t_lyrics* buffer, t_timing_line t)
{
  unsigned int index = 0;
  int deep = 0;
  unsigned int ret = BUFFER_SIZE;

  for (; index < BUFFER_SIZE; ++index)
    if (slide_check(buffer[index], t)
        || credit_check(buffer[index], t))
      return index;
    else if (quick_check(buffer[index], t))
    {
      int d = deep_check(buffer[index], t);
      if (d > deep)
      {
        ret = index;
        deep = d;
      }
    }

  if (ret != BUFFER_SIZE)
    return ret;

  for (index = 0; index < BUFFER_SIZE; ++index)
    if (buffer[index].text[0] == 0)
      return index + BUFFER_SIZE;

  return 2 * BUFFER_SIZE;
}

int
convert_to_ass(t_timing_line* t,
               unsigned int s,
               t_convert_ass* ass)
{
  unsigned int c = 0;
  unsigned int f_time = 1;
  unsigned int f_lyrics = 1;
  t_lyrics* buffer = NULL;
  t_timing_line* cache = NULL;

  // Initialize buffers
  buffer = calloc(BUFFER_SIZE, sizeof (t_lyrics));
  if (buffer == NULL)
  {
    fprintf(stderr, "Cannot allocate time buffer\n");
    return 0;
  }
  cache = calloc(CACHE_SIZE, sizeof (t_timing_line));
  if (cache == NULL)
  {
    fprintf(stderr, "Cannot allocate lyrics buffer\n");
    free(buffer);
    return 0;
  }

  // Main loop
  for (unsigned int index = 0; index < s; ++index)
  {
    if (check_type(t[index].text) == 1)
    {
      save_time(t[index], &(ass->time[ass->i_time]), cache, c);
      ++(ass->i_time);
      if (ass->i_time == MAX_TIME * f_time)
      {
        f_time *= 2;
        ass->time = realloc(ass->time, sizeof(t_time) * MAX_TIME * f_time);
        if (ass->time == NULL)
        {
          free_2(buffer, cache);
          fprintf(stderr, "There are too many time: cannot allocate %u\n", (unsigned int) (f_time * MAX_TIME * sizeof (t_time)));
          return 0;
        }
      }
    }
    else
    {
      unsigned int i = find_index(buffer, t[index]);
      if (i == 2 * BUFFER_SIZE)
      {
        dump_lyrics(ass);
        for (unsigned int rr = 0; rr < 8; ++rr)
          dump_t_lyrics(buffer[rr]);
        free_2(buffer, cache);
        fprintf(stderr, "Cannot find a free buffer place\n");
        return 0;
      }
      else if (i < BUFFER_SIZE)
      {
        int save = 0;
        merge_lyrics(&(buffer[i]), t[index], &save);
        // printf("================\nindex = %i\n", i);
        // dump(t[index]);
        // dump_lyrics(buffer);
        // printf("================\n");
        if ((save == 1
             || (((t[index].color.flag == 2
                   && ALPHA(t[index].color.last) == 0xFF)
                  || (t[index].color.flag == 1
                      && ALPHA(t[index].color.first) == 0xFF))
                 && t[index].text[buffer[i].length - 1] != ' ')))
        {
          save_subtitle(&(ass->lyrics[ass->i_lyrics]), buffer[i]);
          if (save == 1)
          {
            if (ass->lyrics[ass->i_lyrics].character[0][0].free == 1)
              ass->lyrics[ass->i_lyrics].character[0][0].free = 2;
            else
              ass->lyrics[ass->i_lyrics].character[0][0].free = 3;
            free_effect(&(buffer[i]));
          }
          else
            free_buffer_subtitle(&(buffer[i]));
          ++(ass->i_lyrics);
          if (ass->i_lyrics == MAX_LYRICS * f_lyrics)
          {
            f_lyrics *= 2;
            ass->lyrics = realloc(ass->lyrics, sizeof(t_lyrics) * MAX_LYRICS * f_lyrics);
            if (ass->lyrics == NULL)
            {
              free_2(buffer, cache);
              fprintf(stderr,
                      "There are too many lyrics: cannot allocate %u\n",
                      f_lyrics * MAX_LYRICS * sizeof (t_lyrics));
              return 0;
            }
          }
        }
      }
      else
        save_lyrics(&(buffer[i & (BUFFER_SIZE - 1)]), t[index]);

      // Save time in cache
      c = (c + 1) & (CACHE_SIZE - 1);
      put_line(&(cache[c]), t[index]);
    }
  }

  free_2(buffer, cache);
  return 1;
}
