#include "input.h"

void
put_line(t_timing_line* dst, t_timing_line src)
{
  dst->start = src.start;
  dst->stop = src.stop;
  dst->line = src.line;
  dst->color.flag = src.color.flag;
  dst->color.first = src.color.first;
  dst->color.last = src.color.last;
  dst->position.flag = src.position.flag;
  dst->position.first.x = src.position.first.x;
  dst->position.first.y = src.position.first.y;
  dst->position.last.x = src.position.last.x;
  dst->position.last.y = src.position.last.y;
  dst->size.flag = src.size.flag;
  dst->size.first = src.size.first;
  dst->size.last = src.size.last;
  strcpy(dst->text, src.text);
}

static t_timing_line*
get_all_lines(FILE* input, unsigned int* index)
{
  t_timing_line* t = NULL;
  unsigned int factor = 1;
  size_t l = 0;
  char* line = NULL;

  t = calloc(MAX_LINE_TIMING, sizeof (t_timing_line));
  if (t == NULL)
  {
    fprintf(stderr, "Cannot create buffer\n");
    return NULL;
  }

  FILE* lyr = fopen(".default.lyr", "w");
  if (lyr == NULL)
  {
    fprintf(stderr, "Cannot get lyrics from file\n");
    free(t);
    return NULL;
  }

  FILE* frm = fopen(".default.frm", "w");
  if (frm == NULL)
  {
    fprintf(stderr, "Cannot get frames from file\n");
    free(t);
    fclose(lyr);
    return NULL;
  }

  while ((getline(&line, &l, input)) != -1)
  {
    if (*index == (factor * MAX_LINE_TIMING))
    {
      factor *= 2;
      t = realloc(t, factor * MAX_LINE_TIMING * sizeof (t_timing_line));

      if (t == NULL)
      {
        fprintf(stderr, "Cannot reallocate buffer: size of %d\n", factor * MAX_LINE_TIMING);
        return NULL;
      }
    }
    *index = get_timing_line(line, t, *index, lyr, frm);
  }

  fclose(frm);
  fclose(lyr);

  if (line != NULL)
    free(line);

  return t;
}

t_timing_line*
get_timing_lines(const char* file, unsigned int* index)
{
  t_timing_line* timing_line = NULL;
  FILE* input = NULL;

  input = fopen(file, "r");
  if (input == NULL)
  {
    fprintf(stderr, "%s cannot be open\n", file);
    return NULL;
  }

  timing_line = get_all_lines(input, index);
  fclose(input);

  return timing_line;
}
