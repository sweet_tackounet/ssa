#ifndef INPUT_H_
# define INPUT_H_

# include "../convert/main.h"
# include "../sort/sort.h"

void
put_line(t_timing_line* dst, t_timing_line src);
t_timing_line*
get_timing_lines(const char* file, unsigned int* index);

#endif /* INPUT_H_ */
