#ifndef SORT_H_
# define SORT_H_

# include "../convert/main.h"
# include "../modifiers/modifiers.h"
# include "../input/input.h"

unsigned int
get_timing_line(const char* line, t_timing_line* t, unsigned int index, FILE* lyr, FILE* frm);

#endif /* SORT_H_ */
