#include "sort.h"

static inline int
line_cmp(t_timing_line tl1, t_timing_line tl2)
{
  if (tl1.start > tl2.start
      || (tl1.start == tl2.start && tl1.stop > tl2.stop)
      || (tl1.start == tl2.start && tl1.stop == tl2.stop && tl1.line > tl2.line))
    return 1;
  else if (tl1.start == tl2.start
           && tl1.stop == tl2.stop
           && tl1.line == tl2.line
           && tl1.color.flag == tl2.color.flag
           && tl1.color.first == tl2.color.first
           && tl1.color.last == tl2.color.last
           && tl1.size.flag == tl2.size.flag
           && tl1.size.first == tl2.size.first
           && tl1.size.last == tl2.size.last
           && tl1.position.flag == tl2.position.flag
           && tl1.position.first.x == tl2.position.first.x
           && tl1.position.first.y == tl2.position.first.y
           && tl1.position.last.x == tl2.position.last.x
           && tl1.position.last.y == tl2.position.last.y)
    return 0;
  else
    return -1;
}

static inline int
can_merge(const char* text1, const char* text2)
{
  int i = 0;

  for (; text2[i]; ++i)
  {
    if (text1[i] != text2[i])
    {
      if (text1[i] != ' ' && text2[i] != ' ')
        return 0;
    }
  }

  return (text2[i] == text1[i]);
}

static inline void
merge_line(char* text1, const char* text2)
{
  for (int i = 0; text2[i]; ++i)
  {
    if (text1[i] != text2[i] && text2[i] != ' ')
      text1[i] = text2[i];
  }
}

static unsigned int
insert_line(unsigned int index, t_timing_line* t, t_timing_line tmp)
{
  int i = 0;
  int c = 0;

  if (index == 0 || line_cmp(tmp, t[index - 1]) == 1)
  {
    put_line(&(t[index]), tmp);
    return index + 1;
  }

  for (i = index - 1; i >= 0; --i)
    if (line_cmp(tmp, t[i]) == 0)
      do {
        c = i;
        if (can_merge(tmp.text, t[c].text))
        {
          merge_line(t[c].text, tmp.text);
          return index;
        }
        --c;
      } while (i >= 0 && line_cmp(tmp, t[c]) == 0);
    else if (line_cmp(tmp, t[i]) == 1)
      break;

  for (int j = index - 1; j > i; --j)
    put_line(&(t[j + 1]), t[j]);
  put_line(&(t[i + 1]), tmp);

  return index + 1;
}

static inline unsigned int
get_modifier(char* text, t_timing_line* t, unsigned int pos)
{
  switch (text[pos + 1])
  {
    case 'o':
      return get_position(text, t, pos);
    case 'c':
      return get_colors(text, t, pos);
    case 's':
      return get_size(text, t, pos);
    default:
      return 0;
  }
}

static inline void
parse_line(char* text, t_timing_line* t)
{
  unsigned int length = strlen(text);
  unsigned int j = 0;

  for (unsigned int i = 0; i < length; ++i)
    if (text[i] == '{')
      i = get_modifier(text, t, i);
    else if (text[i] == '|')
      ++(t->line);
    else
      t->text[j++] = text[i];
  t->text[j] = 0;
}

unsigned int
get_timing_line(const char* line, t_timing_line* t, unsigned int index, FILE* lyr, FILE* frm)
{
  char text[SIZE_TEXT];
  t_timing_line tmp = init_timing_line();

  if (sscanf(line, "{%u}{%u}%[^\r\n]", &(tmp.start), &(tmp.stop), text) == 3
      && strcmp(text, "{needed at beginning of file for file format recognition by mplayer}"))
  {
    parse_line(text, &tmp);
    return insert_line(index, t, tmp);
  }
  else if (sscanf(line, "--%[^\r\n]", text) == 1)
    fprintf(lyr, "%s\n", text);
  else if (sscanf(line, "--%[\r\n]", text) == 1)
    fprintf(lyr, "\n");
  else if (sscanf(line, "==%u %u", &(tmp.start), &(tmp.stop)) == 2)
    fprintf(frm, "%u %u\n", tmp.start, tmp.stop);

  return index;
}
