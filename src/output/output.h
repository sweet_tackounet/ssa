#ifndef OUTPUT_H_
# define OUTPUT_H_

# include "../convert/handle.h"
# include "function.h"
# include "../file/file.h"

void
write_file(const char* file, t_convert_ass ass, t_handle handle, t_name_handle name);

#endif /* !OUTPUT_H_ */
