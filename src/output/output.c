#include "output.h"

static int g_output_pos[50] =
{
  390, 380, 370, 360, 350,
  340, 330, 321, 311, 301,
  291, 281, 271, 261, 251,
  242, 232, 222, 212, 202,
  192, 182, 172, 163, 153,
  143, 133, 123, 113, 103,
   93,  84,  74,  64,  54,
   44,  34,  24,  14,   5,
   -4, -14, -24, -34, -44,
  -54, -63, -73, -83, -93
};

static inline void
free_extended_effect(t_extended_lyrics* t)
{
  for (unsigned int i = 0; i < MAX_LEN; ++i)
    free(t->character[i]);
  free(t->character);
  free(t);
}

static inline t_extended_lyrics*
init_extended_lyrics(unsigned int start, unsigned int stop)
{
  t_extended_lyrics* t = calloc(1, sizeof (t_extended_lyrics));
  t->character = calloc(MAX_LEN, sizeof (t_character_effect*));

  t->start = start;
  t->stop = stop;

  for (unsigned int i = 0; i < MAX_LEN; ++i)
    t->character[i] = calloc(MAX_TOTAL_EFFECT, sizeof (t_character_effect));

  return t;
}

void
save_character(t_character_effect* c, t_character_effect t)
{
  c->free = 1;
  c->start = t.start;
  c->stop = t.stop;
  c->color.flag = t.color.flag;
  c->color.first = t.color.first;
  c->color.last = t.color.last;
  c->size.flag = t.size.flag;
  c->size.first = t.size.first;
  c->size.last = t.size.last;
  c->position.flag = t.position.flag;
  c->position.first.x = t.position.first.x;
  c->position.first.y = t.position.first.y;
  c->position.last.x = t.position.last.x;
  c->position.last.y = t.position.last.y;
}

static t_extended_lyrics*
fusion_lyrics(t_convert_ass ass, int* record, char* text, unsigned int length)
{
  int rec = record[0];
  t_extended_lyrics* ret = init_extended_lyrics(ass.lyrics[rec].start, ass.lyrics[rec].stop);
  int* index = calloc(MAX_LEN, sizeof (int));
  if (index == NULL)
  {
    fprintf(stderr, "Cannot malloc all index: [%s]\n", text);
    return ret;
  }

  for (int i = 0; i < MAX_LEN; ++i)
    index[i] = 0;

  for (unsigned int c = 0; c < length; ++c)
    if (text[c] != ' ')
      for (int a = 0; a < EXTEND_EFFECT && record[a] != -1; ++a)
      {
        t_lyrics t = ass.lyrics[record[a]];
        if (ret->stop < t.stop)
          ret->stop = t.stop;
        if (ret->start > t.start)
          ret->start = t.start;
        for (unsigned int i = index[c], j = 0; i < MAX_TOTAL_EFFECT && j < MAX_EFFECT; ++j)
          if (t.character[c][j].free != 0
              && t.character[c][j].free != 5
              && (i == 0
                  || ret->character[c][i - 1].color.flag != t.character[c][j].color.flag
                  || ret->character[c][i - 1].color.first != t.character[c][j].color.first
                  || ret->character[c][i - 1].color.last != t.character[c][j].color.last
                  || ret->character[c][i - 1].size.flag != t.character[c][j].size.flag
                  || ret->character[c][i - 1].size.first != t.character[c][j].size.first
                  || ret->character[c][i - 1].size.last != t.character[c][j].size.last
                  || ret->character[c][i - 1].position.flag != t.character[c][j].position.flag
                  || ret->character[c][i - 1].position.first.x != t.character[c][j].position.first.x
                  || ret->character[c][i - 1].position.first.y != t.character[c][j].position.first.y
                  || ret->character[c][i - 1].position.last.x != t.character[c][j].position.last.x
                  || ret->character[c][i - 1].position.last.y != t.character[c][j].position.last.y))
          {
            save_character(&(ret->character[c][i]), t.character[c][j]);
            ++i;
            ++(index[c]);
            if (i == MAX_TOTAL_EFFECT - 1)
              fprintf(stderr, "No more effect: %u %c[%s]\n", c, text[c], text);
          }
      }

  free(index);
  return ret;
}

static inline void
writeheader(FILE* out, const char* title, t_handle handle, t_name_handle name)
{
  fprintf(out,
          "[Script Info]\nTitle: %s\nScriptType: v4.00+\nPlayResX: 640\nPlayResY: 480\nScaledBorderAndShadow: yes\n[V4+ Styles]\nFormat: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding\nStyle: D,Nu Sans Mono Demo,%.2f,0,0,0,0,1,0,0,0,100,100,0,0,1,1,1,8,0,0,0,1\nStyle: C,Nu Sans Mono Demo,%.2f,0,0,0,0,1,0,0,0,100,100,0,0,1,1,1,1,0,0,0,1\nStyle: L,Arial,1,&HFF,0,0,0,1,0,0,0,%d,%d,0,0,1,1,1,8,0,0,0,1\n\n[Events]\nFormat: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text\nComment:<l>",
          title,
          handle.font,
          handle.font * 0.8,
          handle.logo_size,
          handle.logo_size);

  FILE* lyr = NULL;
  FILE* frm = NULL;
  lyr = fopen(".default.lyr", "r");
  frm = fopen(".default.frm", "r");
  ssize_t read = 0;
  size_t l = 0;
  float fps = handle.fps;
  char* line = NULL;
  int m = 0;

  if (lyr == NULL)
    fprintf(stderr, "Cannot open lyrics file\n");
  else
  {
    while ((read = getline(&line, &l, lyr)) != -1)
    {
      line[read - 1] = 0;
      if (m == 1)
        fprintf(out, "|%s", line);
      else
      {
        fprintf(out, "%s", line);
        m = 1;
      }
    }
    fclose(lyr);
  }
  fprintf(out, "</l><f>");

  if (frm == NULL)
    fprintf(stderr, "Cannot open lyrics file\n");
  else
  {
    m = 0;
    while ((read = getline(&line, &l, frm)) != -1)
    {
      unsigned int start = 0;
      unsigned int stop = 0;
      line[read - 1] = 0;
      if (sscanf(line, "%u %u", &start, &stop) == 2)
      {
        if (m == 1)
          fprintf(out, "|%u %u", getms(start, fps), getms(stop, fps));
        else
        {
          fprintf(out, "%u %u", getms(start, fps), getms(stop, fps));
          m = 1;
        }
      }
    }
    fclose(frm);
  }
  fprintf(out, "</f>\n");
  if (line != NULL)
    free(line);

  if (name.save == 1 && name.dst != NULL && m != 0)
  {
    int l_dst = strlen(name.dst);
    int l_title = strlen(title);
    char* f = calloc(l_dst + l_title + 6, sizeof (char));
    if (f == NULL)
    {
      fprintf(stderr, "Cannot get path for lyrics/frame for %s\n", title);
      remove(".default.lyr");
      remove(".default.frm");
      free(f);
      return;
    }
    strcpy(f, name.dst);
    f[l_dst] = '/';
    strcat(f, title);
    strcat(f, ".lyr");
    if (rename(".default.lyr", f) == -1)
    {
      fprintf(stderr, "Cannot write lyrics %s\n", f);
      remove(".default.lyr");
    }
    f[l_dst + l_title + 1] = 0;
    strcat(f, ".frm");
    if (rename(".default.frm", f) == -1)
    {
      fprintf(stderr, "Cannot write frame %s\n", f);
      remove(".default.frm");
    }
    free(f);
  }
  else
  {
    remove(".default.lyr");
    remove(".default.frm");
  }
}

static FILE*
get_output(const char* file, t_handle handle, t_name_handle name)
{
  int n = strlen(file);
  if (n < 4)
  {
    fprintf(stderr, "Check argument name: %s\n", file);
    return NULL;
  }

  char* title = get_filename(file);
  if (title == NULL)
  {
    fprintf(stderr, "Cannot get title: %s\n", file);
    return NULL;
  }
  FILE* out = get_path_file(file, name.dst, ".ass", "w");
  if (out == NULL)
  {
    fprintf(stderr, "Cannot write ass: %s\n", file);
    free(title);
    return NULL;
  }
  writeheader(out, title, handle, name);
  free(title);

  return out;
}

static int
write_effect(FILE* out,
             t_handle handle,
             t_character_effect ce,
             t_previous_data* p,
             unsigned int length)
{
  int ret = 0;
  float fps = handle.fps;
  int change = 0;

  // Ignore transparent case
  if (ce.color.flag != 0
      && ce.color.flag != 2 
      && (ALPHA(ce.color.first)) == 0xFF)
  {
    p->color = ce.color.first;
    p->pos = ce.position.flag;
    p->x_pos = ce.position.first.x;
    p->y_pos = ce.position.first.y;
    return 0;
  }

  // Set color
  if (ce.color.flag >= 1)
  {
    if (ce.color.flag < 3 && ALPHA(ce.color.first) != ALPHA(p->color))
    {
      fprintf(out,
              "\\t(%u,%u,\\alpha&%02X&",
              getms(ce.start - p->start, fps),
              getms(ce.start - p->start, fps),
              ALPHA(ce.color.first));
      ++ret;
      change = 1;
    }
    if (COLOR(p->color) != COLOR(ce.color.first))
    {
      if (change == 1)
        fprintf(out,
                "\\c&%06lX&",
                COLOR(ce.color.first));
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\c&%06lX&",
                getms(ce.start - p->start, fps),
                getms(ce.start - p->start, fps),
                COLOR(ce.color.first));
        ++ret;
        change = 1;
      }
    }
    p->color = ce.color.first;
  }

  if ((ce.position.flag & ~8) != 0 && (p->pos & ~8) == 0)
  {
    if (ce.position.first.x != g_output_pos[length - 1])
    {
      if (change == 1)
        fprintf(out,
                "\\frz%.4g",
                (ce.position.first.x - g_output_pos[length - 1]) * handle.move_x);
      else
      {
        fprintf(out, "\\t(%u,%u,\\frz%.4g",
                getms(ce.start - p->start, fps),
                getms(ce.start - p->start, fps),
                (ce.position.first.x - g_output_pos[length - 1]) * handle.move_x);
        ++ret;
        change = 1;
      }
    }
    if (ce.position.first.y != 0)
    {
      if (change == 1)
        fprintf(out,
                "\\frx%.8g",
                ce.position.first.y * handle.move_y);
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\frx%.8g",
                getms(ce.start - p->start, fps),
                getms(ce.start - p->start, fps),
                ce.position.first.y * handle.move_y);
        ++ret;
        change = 1;
      }
    }
  }

  if ((ce.position.flag & ~8) == 0 && (p->pos & ~8) != 0)
  {
    if (p->x_pos != g_output_pos[length - 1])
    {
      if (change == 1)
        fprintf(out, "\\frz0");
      else
      {
        fprintf(out, "\\t(%u,%u,\\frz0",
                getms(ce.start - p->start, fps),
                getms(ce.start - p->start, fps));
        ++ret;
        change = 1;
      }
    }
    if (p->y_pos != 0)
    {
      if (change == 1)
        fprintf(out, "\\frx0");
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\frx0",
                getms(ce.start - p->start, fps),
                getms(ce.start - p->start, fps));
        ++ret;
      }
    }
  }
  change = 0;

  if (ce.color.flag == 2)
  {
    if (ALPHA(ce.color.first) != ALPHA(ce.color.last))
    {
      fprintf(out,
              "\\t(%u,%u,\\alpha&%02X&",
              getms(ce.start - p->start, fps),
              getms(ce.stop - p->start, fps),
              ALPHA(ce.color.last));
      ++ret;
      change = 1;
    }
    if (COLOR(ce.color.first) != COLOR(ce.color.last))
    {
      if (change == 1)
        fprintf(out,
                "\\c&%06lX&",
                COLOR(ce.color.last));
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\c&%06lX&",
                getms(ce.start - p->start, fps),
                getms(ce.stop - p->start, fps),
                COLOR(ce.color.last));
        ++ret;
      }
    }
    p->color = ce.color.last;
  }

  if (ce.color.flag > 2)
  {
    fprintf(out,
            "\\t(%u,%u,1.9,\\alpha&%02X&",
            getms(ce.color.flag - 3 - p->start, fps),
            getms(ce.stop - p->start, fps),
            ALPHA(ce.color.last));
    ++ret;
    change = 1;
    p->color = ce.color.last;
  }

  if ((ce.position.flag & ~8) >= 1)
  {
    if (ce.position.first.x != p->x_pos)
    {
      if (change == 1)
        fprintf(out,
                "\\frz%.4g",
                (ce.position.first.x - g_output_pos[length - 1]) * handle.move_x);
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\frz%.4g",
                getms(ce.start - p->start, fps),
                getms(ce.stop - p->start, fps),
                (ce.position.first.x - g_output_pos[length - 1]) * handle.move_x);
        ++ret;
        change = 1;
      }
    }
    if (ce.position.first.y != p->y_pos)
    {
      if (change == 1)
      {
        fprintf(out,
                "\\frx%.8g",
                ce.position.first.y * handle.move_y);
      }
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\frx%.8g",
                getms(ce.start - p->start, fps),
                getms(ce.stop - p->start, fps),
                ce.position.first.y * handle.move_y);
        ++ret;
        change = 1;
      }
    }
  }
  else if ((ce.position.flag & ~32) == 32)
  {
    fprintf(out,
            "\\t(%u,%u,\\frz%.4g",
            getms(ce.position.rx - p->start, fps),
            getms(ce.stop - p->start, fps),
            (ce.position.first.x - g_output_pos[length - 1]) * handle.move_x);
    ++ret;
  }
  else if ((ce.position.flag & ~64) == 64)
  {
    fprintf(out,
            "\\t(%u,%u,\\frx%.8g",
            getms(ce.position.ry - p->start, fps),
            getms(ce.stop - p->start, fps),
            ce.position.first.y * handle.move_y);
    ++ret;
  }
  else if ((ce.position.flag & ~8) == 0
           && (p->pos & ~8) != 0)
  {
    fprintf(out,
            "\\t(%u,%u,\\frz0\\frx0",
            getms(ce.start - p->start, fps),
            getms(ce.stop - p->start, fps));
    ++ret;
    change = 1;
  }

  p->x_pos = ce.position.first.x;
  p->y_pos = ce.position.first.y;
  p->pos = ce.position.flag;

  return ret;
}

static inline int
slider(t_lyrics lyrics)
{
  return (lyrics.character[0][0].position.flag == 4);
}

static unsigned int
write_slider(FILE* out,
             t_lyrics lyrics,
             t_handle handle,
             t_time* time,
             unsigned int l,
             unsigned int i_time)
{
  float fps = handle.fps;
  unsigned int length = lyrics.length;
  t_color color =  COLOR(lyrics.character[0][0].color.first);
  unsigned int start = lyrics.character[0][1].start;
  unsigned int stop = lyrics.character[0][2].start;
  float delay = (stop - start) * 1.4;
  t_color color_time = COLOR(lyrics.character[0][1].color.first);
  t_color color_end = COLOR(lyrics.character[0][1].color.last);
  float move = (stop - start) * handle.move_angle; // 5.638 * (handle.angle_logo/handle.space)
  unsigned int s0 = start - 60;
  unsigned int s1 = start - 50;
  unsigned int s2 = start - 40;
  unsigned int s3 = start - 30;
  unsigned int s4 = start - 20;
  unsigned int s5 = start - 10;
  unsigned int height = lyrics.line * handle.height_line + handle.margin_sub;
  float max_angle = handle.max_angle;
  float angle = handle.angle_logo;

  fprintf(out,
          "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\c&%06lX&\\alpha&FF&\\t(0,%u,\\alpha&7F&)}%s\nDialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\c&%06lX&\\alpha&7F&\\t(0,%u,\\alpha&3F&)}%s\nDialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\c&%06lX&\\alpha&3F&\\t(0,%u,\\alpha&15&)}%s\nDialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\c&%06lX&\\alpha&15&\\t(0,%u,\\alpha&00&)}%s\nDialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\c&%06lX&\\alpha&00&}%s\n",
          gethour(s0, fps),
          getmin(s0, fps),
          getsec(s0, fps),
          getsh(s0, fps),
          gethour(s1, fps),
          getmin(s1, fps),
          getsec(s1, fps),
          getsh(s1, fps),
          (unsigned int) (658 + delay),
          height,
          (unsigned int) (558 + delay),
          height,
          color,
          getms(10, fps),
          lyrics.text,
          gethour(s1, fps),
          getmin(s1, fps),
          getsec(s1, fps),
          getsh(s1, fps),
          gethour(s2, fps),
          getmin(s2, fps),
          getsec(s2, fps),
          getsh(s2, fps),
          (unsigned int) (558 + delay),
          height,
          (unsigned int) (483 + delay),
          height,
          color,
          getms(10, fps),
          lyrics.text,
          gethour(s2, fps),
          getmin(s2, fps),
          getsec(s2, fps),
          getsh(s2, fps),
          gethour(s3, fps),
          getmin(s3, fps),
          getsec(s3, fps),
          getsh(s3, fps),
          (unsigned int) (483 + delay),
          height,
          (unsigned int) (423 + delay),
          height,
          color,
          getms(10, fps),
          lyrics.text,
          gethour(s3, fps),
          getmin(s3, fps),
          getsec(s3, fps),
          getsh(s3, fps),
          gethour(s4, fps),
          getmin(s4, fps),
          getsec(s4, fps),
          getsh(s4, fps),
          (unsigned int) (423 + delay),
          height,
          (unsigned int) (378 + delay),
          height,
          color,
          getms(10, fps),
          lyrics.text,
          gethour(s4, fps),
          getmin(s4, fps),
          getsec(s4, fps),
          getsh(s4, fps),
          gethour(s5, fps),
          getmin(s5, fps),
          getsec(s5, fps),
          getsh(s5, fps),
          (unsigned int) (378 + delay),
          height,
          (unsigned int) (348 + delay),
          height,
          color,
          lyrics.text);

  for (unsigned int i = 0; i < length; ++i)
  {
    if (lyrics.character[i][1].free != 0)
    {
      if (i != 0)
        fprintf(out, "{\\alpha&00&\\c&%06lX&", color);
      else
        fprintf(out,
                "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\alpha&00&\\c&%06lX&",
                gethour(s5, fps),
                getmin(s5, fps),
                getsec(s5, fps),
                getsh(s5, fps),
                gethour(stop, fps),
                getmin(stop, fps),
                getsec(stop, fps),
                getsh(stop, fps),
                (unsigned int) (348 + delay),
                height,
                (unsigned int) (320 - delay),
                height,
                color);

      fprintf(out,
              "\\t(%u,%u,\\c&%06lX&\\t(%u,%u,\\c&%06lX&\\t(%u,%u,\\alpha&58&)))}",
              getms(lyrics.character[i][1].start - s5, fps),
              getms(lyrics.character[i][1].start - s5, fps),
              color_time,
              getms(lyrics.character[i][1].start - s5, fps),
              getms(lyrics.character[i][1].stop - s5, fps),
              color_end,
              (int) (lyrics.character[i][1].stop + 10 + 1000 / fps),
              (int) (lyrics.character[i][1].stop + 10 + 30000 / fps));
    }
    fprintf(out, "%c", lyrics.text[i]);
  }
  fprintf(out, "\n");
  s0 = stop + 8;
  fprintf(out,
          "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,0,,{\\move(%u,%u,%u,%u)\\c&%06lX&\\alpha&58&\\t(%u,%u,\\alpha&FF&)}%s\n",
          gethour(stop, fps),
          getmin(stop, fps),
          getsec(stop, fps),
          getsh(stop, fps),
          gethour(s0, fps),
          getmin(s0, fps),
          getsec(s0, fps),
          getsh(s0, fps),
          (unsigned int) (320 - delay),
          height,
          (unsigned int) (298 - delay),
          height,
          color_end,
          getms(3, fps),
          getms(8, fps),
          lyrics.text);

  int update = 0;
  float logo_time[50] =
  {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1
  };
  int logo_start[50] =
    {
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    };
  int logo_stop[50] =
    {
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    };
  unsigned int t_logo = 0;
  int y = 0;
  s1 = time[l].start;
  s2 = time[l].stop;
  s3 = stop - start;
  while (l < i_time
         && s2 <= stop)
  {
    if (time[l].length == length
        && time[l].line == lyrics.line + 1)
    {
      logo_start[t_logo] = s1;
      logo_stop[t_logo] = s2;
      logo_time[t_logo] = max_angle - (49 - time[l].length + time[l].characters + 2 * time[l].hit) * angle + (move / 2);      
      if (y == 0)
        y = time[l].line * handle.height_line + handle.margin_logo;
      ++t_logo;
    }
    else if (update == 0)
      update = l;
    ++l;
    s1 = time[l].start;
    s2 = time[l].stop;
  }
  if (update != 0)
    l = update;

  if (t_logo > 0)
  {
    int p = 1;
    fprintf(out,
            "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,L,,0,0,%i,,{\\org(320,100000)\\frz%.8g\\t(0,%i,\\frz%.8g",
            gethour(logo_start[0], fps),
            getmin(logo_start[0], fps),
            getsec(logo_start[0], fps),
            getsh(logo_start[0], fps),
            gethour(logo_stop[t_logo - 1], fps),
            getmin(logo_stop[t_logo - 1], fps),
            getsec(logo_stop[t_logo - 1], fps),
            getsh(logo_stop[t_logo - 1], fps),
            y,
            logo_time[0] - (move * (stop - logo_start[0])) / (float) (s3),
            getms(logo_stop[0] - start, fps),
            logo_time[0] - (move * (stop - logo_stop[0])) / (float) (s3));
    s1 = logo_start[0];
    for (unsigned int i = 1; i < t_logo; ++i)
      if (logo_start[i] != logo_stop[i - 1])
      {
        fprintf(out,
                "\\t(%u,%u,\\alpha&FF&\\t(%u,%u,\\alpha&00&\\frz%.8g\\t(%u,%u,\\frz%.8g",
                getms(logo_stop[i - 1] - s1, fps),
                getms(logo_stop[i - 1] - s1, fps),
                getms(logo_start[i] - s1, fps),
                getms(logo_start[i] - s1, fps),
                logo_time[i] - (move * (stop - logo_start[i])) / (float) (s3),
                getms(logo_start[i] - s1, fps),
                getms(logo_stop[i] - s1, fps),
                logo_time[i] - (move * (stop - logo_stop[i])) / (float) (s3));
        p += 3;
      }
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\frz%.8g\\t(%u,%u,\\frz%.8g",
                getms(logo_start[i] - s1, fps),
                getms(logo_start[i] - s1, fps),
                logo_time[i] - (move * (stop - logo_start[i])) / (float) (s3),
                getms(logo_start[i] - s1, fps),
                getms(logo_stop[i] - s1, fps),
                logo_time[i] - (move * (stop - logo_stop[i])) / (float) (s3));
        p += 2;
      }
    for (int i = 0; i < p; ++i)
      fprintf(out, ")");
    fprintf(out,
            "\\p1}m 0 0 s 29 0 29 29 0 29 c{\\p0\\1c&FFFF11&\\p1}m -22 6 b -26 5 -27 0 -24 -4 -20 -10 -14 -9 -12 -8 -10 -7 -12 -6 -14 -4 -16 -2 -16 0 -16 1 -16 3 -18 7 -22 6{\\p0\\p1}m -28 -23.8 s -27 -23.8 -27 -22.8 -28 -22.8 c{\\p0\\p1}m -29.5 -19 b -29.5 -21 -28 -21 -25 -19 -28 -19 -29.5 -18 -29.5 -19{\\p0\\1c&81DAF5&\\p1}m -36 2 b -35 -2 -33 -3 -31 -3 -29 -3 -25 -1 -27 5 -28 8 -31 11 -34 12 -34 12 -33 10 -33 10 -35 13 -40 12 -42 11 -43 10 -44 11 -40 9 -38 8 -37 6 -36 2{\\p0\\p1}m -65 -1 b -63 2 -59 1 -58 -2 -57 -5 -59 -1 -61 -1 -62 -1 -62 -1 -63 -1 -62 0 -60 0 -65 -1{\\p0}\n");
  }

  return l;
}

static void
write_credit(FILE* out, t_lyrics lyrics, t_handle handle)
{
  float fps = handle.fps;
  fprintf(out,
          "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,C,,40,0,20,,{\\c&%06lX&\\alpha&%02X&\\t(0,%i,\\alpha&%02X&\\t(%u,%u,\\alpha&%02X&))}%s\n",
          gethour(lyrics.start, fps),
          getmin(lyrics.start, fps),
          getsec(lyrics.start, fps),
          getsh(lyrics.start, fps),
          gethour(lyrics.stop, fps),
          getmin(lyrics.stop, fps),
          getsec(lyrics.stop, fps),
          getsh(lyrics.stop, fps),
          lyrics.character[0][0].color.first & 0xFFFFFF,
          ALPHA(lyrics.character[0][0].color.first),
          getms(lyrics.character[0][0].stop - lyrics.start, fps),
          ALPHA(lyrics.character[0][1].color.first),
          getms(lyrics.character[0][2].start - lyrics.start, fps),
          getms(lyrics.character[0][2].stop - lyrics.start, fps),
          ALPHA(lyrics.character[0][2].color.first),
          lyrics.text);
}

static inline int
several_effect(t_lyrics lyrics)
{
  return (lyrics.character[0][0].free == 2
          || lyrics.character[0][0].free == 3);
}

static unsigned int
write_several_effect(FILE* out,
                     t_convert_ass ass,
                     unsigned int index,
                     unsigned int i_lyrics,
                     t_handle handle,
                     unsigned int l,
                     unsigned int i_time)
{
  unsigned int ret = 0;
  char* text = calloc(MAX_LEN, sizeof(char));
  float max_angle = handle.max_angle;
  float angle = handle.angle_logo;
  if (text == NULL)
  {
    fprintf(stderr, "Cannot calloc text for extend effects: <%u> {%s}", index, ass.lyrics[index].text);
    return index;
  }
  int* record = calloc(EXTEND_EFFECT, sizeof(int));
  if (record == NULL)
  {
    free(text);
    fprintf(stderr, "Cannot calloc record for extend effects: <%u> {%s}", index, ass.lyrics[index].text);
    return index;
  }
  unsigned int x = 0;
  int rec = 1;
  t_lyrics tmp = ass.lyrics[index];
  unsigned int length = tmp.length;
  unsigned int line = tmp.line;
  if (ass.lyrics[index].character[0][0].free == 2)
    ass.lyrics[index].character[0][0].free = 4;
  else if (ass.lyrics[index].character[0][0].free == 3)
    ass.lyrics[index].character[0][0].free = 5;
  else
    fprintf(stderr, "c'est chelou pour l'extend\n");
  int loop = 1;
  record[0] = index;
  for (unsigned int i = 1; i < EXTEND_EFFECT; ++i)
    record[i] = -1;

  // Get all sequences
  for (unsigned int i = index + 1; i < i_lyrics && loop == 1; ++i)
  {
    rec = 1;
    if (ass.lyrics[i].length == length
        && ass.lyrics[i].line == line
        && ass.lyrics[i].character[0][0].free != 4
        && ass.lyrics[i].character[0][0].free != 5)
    {
      for (unsigned int c = 0; c < length; ++c)
      {
        if (tmp.text[c] != ' ' && ass.lyrics[i].text[c] != ' ')
        {
          if (tmp.text[c] != ass.lyrics[i].text[c])
          {
            if (ret == 0)
              ret = i;
            rec = 0;
            break;
          }
          else
          {
            for (unsigned int e = 0;
                 e < MAX_EFFECT
                   && tmp.character[c][e].free != 0
                   && tmp.character[c][e].free != 3
                   && ass.lyrics[i].character[c][e].free != 0
                   && ass.lyrics[i].character[c][e].free != 3
                   && tmp.character[c][e].start != tmp.character[c][e].stop;
                 ++e)
              if ((e == MAX_EFFECT - 1
                   || tmp.character[c][e + 1].free == 0)
                  && (ass.lyrics[i].character[c][0].start != tmp.character[c][e].stop))
              {
                if (ret == 0)
                  ret = i;
                rec = 0;
                break;
              }
          }
        }
      }
      if (ret == 0)
        ret = i;
      if (rec == 1)
      {
        if (ass.lyrics[i].character[0][0].free != 2
            && ass.lyrics[i].character[0][0].free != 3)
          loop = 0;
        if (ass.lyrics[i].character[0][0].free == 2
            || ass.lyrics[i].character[0][0].free == 1)
          ass.lyrics[i].character[0][0].free = 4;
        else if (ass.lyrics[i].character[0][0].free == 3
                 || ass.lyrics[i].character[0][0].free == 0)
          ass.lyrics[i].character[0][0].free = 5;
        record[++x] = i;

        // Copy full text
        for (unsigned int l = 0; l < length; ++l)
          if (tmp.text[l] != ' ')
            text[l] = tmp.text[l];
          else if (ass.lyrics[i].text[l] != ' ')
            text[l] = ass.lyrics[i].text[l];
          else
            text[l] = ' ';

        // Update previous effects
        tmp = ass.lyrics[i];
      }
    }
  }

  length = strlen(text);
  t_extended_lyrics* el = fusion_lyrics(ass, record, text, length);
  int position = 0;

  for (unsigned int i = 0; record[i] != -1; ++i)
    if (ass.lyrics[record[i]].character[0][0].position.flag > 7)
    {
      position = 1;
      break;
    }

  int ssa = 0;
  int diff_alpha = 0;
  int alpha_delta = 0;
  int sspx = 0;
  int diff_posx = 0;
  int posx_delta = 0;
  int sspy = 0;
  int diff_posy = 0;
  int posy_delta = 0;
  t_character_effect ce = el->character[0][1];

  for (unsigned int c = 0; c < length; ++c)
    for (unsigned int e = 1; el->character[c][e].free != 0; ce = el->character[c][++e])
    {
      if (el->character[c][e].color.flag == 1
          && el->character[c][e - 1].color.flag == 1
          && COLOR(el->character[c][e - 1].color.first) == COLOR(el->character[c][e].color.first))
      {
        if (diff_alpha == 0)
        {
          diff_alpha = ALPHA(el->character[c][e].color.first) - ALPHA(el->character[c][e - 1].color.first);
          ssa = el->character[c][0].start;
        }
        else if (diff_alpha == ALPHA(el->character[c][e].color.first) - ALPHA(el->character[c][e - 1].color.first)
                 || diff_alpha == ALPHA(el->character[c][e].color.first) - ALPHA(el->character[c][e - 1].color.first) - alpha_delta)
          el->character[c][e - 1].color.flag = 0;
        else if (alpha_delta == 0
                 && (ALPHA(el->character[c][e].color.first) - ALPHA(el->character[c][e - 1].color.first) - diff_alpha == 1
                     || ALPHA(el->character[c][e].color.first) - ALPHA(el->character[c][e - 1].color.first) - diff_alpha == -1))
        {
          alpha_delta = ALPHA(el->character[c][e].color.first) - ALPHA(el->character[c][e - 1].color.first) - diff_alpha;
          el->character[c][e - 1].color.flag = 0;
        }
        else if (e > 2
                && ALPHA(el->character[c][e].color.first) == ALPHA(el->character[c][e - 1].color.first)
                && ALPHA(el->character[c][e - 2].color.flag == 0))
        {
          el->character[c][e - 1].color.flag = 3 + ssa;
          el->character[c][e - 1].color.last = el->character[c][e - 1].color.first;
          el->character[c][e - 1].color.first = el->character[c][e].color.first;
          diff_alpha = 0;
          alpha_delta = 0;
          ssa = 0;
        }
        else
        {
          diff_alpha = 0;
          alpha_delta = 0;
          ssa = 0;
        }
      }
      else
      {
        diff_alpha = 0;
        alpha_delta = 0;
        ssa = 0;
      }

      if (el->character[c][e].position.flag == 1
          && el->character[c][e - 1].position.flag == 1)
      {
        // Position x
        if (diff_posx == 0)
        {
          diff_posx = el->character[c][e].position.first.x - el->character[c][e - 1].position.first.x;
          sspx = el->character[c][0].start;
        }
        else if (diff_posx == el->character[c][e].position.first.x - el->character[c][e - 1].position.first.x
                 || diff_posx == el->character[c][e].position.first.x - el->character[c][e - 1].position.first.x - posx_delta)
          el->character[c][e - 1].position.flag = 16;
        else if (posx_delta == 0
                 && (el->character[c][e].position.first.x - el->character[c][e - 1].position.first.x - diff_posx == 1
                     || el->character[c][e].position.first.x - el->character[c][e - 1].position.first.x - diff_posx == -1))
        {
          posx_delta = el->character[c][e].position.first.x - el->character[c][e - 1].position.first.x - diff_posx;
          el->character[c][e - 1].position.flag = 16;
        }
        else
        {
          el->character[c][e - 1].position.flag = 32;
          el->character[c][e - 1].position.rx = sspx;
          el->character[c][e - 1].position.last.x = el->character[c][e - 1].position.first.x;
          el->character[c][e - 1].position.first.x = el->character[c][e].position.first.x;
          diff_posx = 0;
          posx_delta = 0;
          sspx = 0;
        }

        // Position y
        if (diff_posy == 0)
        {
          diff_posy = el->character[c][e].position.first.y - el->character[c][e - 1].position.first.y;
          sspy = el->character[c][0].start;
        }
        else if (diff_posy == el->character[c][e].position.first.y - el->character[c][e - 1].position.first.y
                 || diff_posy == el->character[c][e].position.first.y - el->character[c][e - 1].position.first.y - posy_delta)
          el->character[c][e - 1].position.flag = 16;
        else if (posy_delta == 0
                 && (el->character[c][e].position.first.y - el->character[c][e - 1].position.first.y - diff_posy == 1
                     || el->character[c][e].position.first.y - el->character[c][e - 1].position.first.y - diff_posy == -1))
        {
          posy_delta = el->character[c][e].position.first.y - el->character[c][e - 1].position.first.y - diff_posy;
          el->character[c][e - 1].position.flag = 16;
        }
        else
        {
          el->character[c][e - 1].position.flag = 64;
          el->character[c][e - 1].position.ry = sspy;
          el->character[c][e - 1].position.last.y = el->character[c][e - 1].position.first.y;
          el->character[c][e - 1].position.first.y = el->character[c][e].position.first.y;
          diff_posy = 0;
          posy_delta = 0;
          sspy = 0;
        }
      }
      else if (el->character[c][e].position.flag == 0
               && el->character[c][e - 1].position.flag == 1
               && (sspx != 0
                   || sspy != 0
                   || diff_posx != 0
                   || diff_posy != 0
                   || posx_delta != 0
                   || posy_delta != 0))

      {
        el->character[c][e - 1].position.flag = 96;
        el->character[c][e - 1].position.rx = sspx;
        el->character[c][e - 1].position.ry = sspy;
        el->character[c][e - 1].position.last.x = el->character[c][e - 1].position.first.x;
        el->character[c][e - 1].position.last.y = el->character[c][e - 1].position.first.y;
        el->character[c][e - 1].position.first.x = el->character[c][e].position.first.x;
        el->character[c][e - 1].position.first.y = el->character[c][e].position.first.y;
        diff_posx = 0;
        diff_posy = 0;
        posx_delta = 0;
        posy_delta = 0;
        sspx = 0;
        sspy = 0;
      }
    }

  unsigned int start = el->start;
  unsigned int stop = el->stop;
  float fps = handle.fps;

  for (unsigned int c = 0; c < length; ++c)
  {
    t_previous_data p =
      {
        .start = start,
        .color = el->character[0][0].color.first,
        .x_pos = el->character[0][0].position.first.x,
        .y_pos = el->character[0][0].position.first.y,
        .pos = el->character[0][0].position.flag
      };
    if (c == 0)
    {
      if (position == 1)
        if (p.pos == 0)
          fprintf(out,
                  "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,%i,,{\\org(320,10000)\\c&%06lX&\\alpha&%02X&\\frz0\\frx0",
                  gethour(start, fps),
                  getmin(start, fps),
                  getsec(start, fps),
                  getsh(start, fps),
                  gethour(stop, fps),
                  getmin(stop, fps),
                  getsec(stop, fps),
                  getsh(stop, fps),
                  (int) (line * handle.height_line + handle.margin_sub),
                  COLOR(p.color),
                  (ALPHA(p.color) > 0xF8) ? 0xFF : ALPHA(p.color));
        else
          fprintf(out,
                  "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,%i,,{\\org(320,10000)\\c&%06lX&\\alpha&%02X&\\frz%.4g\\frx%.8g",
                  gethour(start, fps),
                  getmin(start, fps),
                  getsec(start, fps),
                  getsh(start, fps),
                  gethour(stop, fps),
                  getmin(stop, fps),
                  getsec(stop, fps),
                  getsh(stop, fps),
                  (int) (line * handle.height_line + handle.margin_sub),
                  COLOR(p.color),
                  (ALPHA(p.color) > 0xF8) ? 0xFF : ALPHA(p.color),
                  (g_output_pos[length - 1] - p.x_pos) * handle.move_x,
                  p.y_pos * handle.move_y);
      else
        fprintf(out,
                "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,%i,,{\\c&%06lX&\\alpha&%02X&",
                gethour(start, fps),
                getmin(start, fps),
                getsec(start, fps),
                getsh(start, fps),
                gethour(stop, fps),
                getmin(stop, fps),
                getsec(stop, fps),
                getsh(stop, fps),
                (int) (line * handle.height_line + handle.margin_sub),
                COLOR(p.color),
                (ALPHA(p.color) > 0xF8) ? 0xFF : ALPHA(p.color));
    }
    if (text[c] != ' ')
    {
      int k = 0;
      ce = el->character[c][0];

      if (c != 0)
      {
        if (position == 1)
          if ((ce.position.flag & ~8) == 0)
            fprintf(out,
                    "{\\c&%06lX&\\alpha&%02X&\\frz0\\frx0",
                    COLOR(ce.color.first),
                    (ALPHA(ce.color.first) > 0xF8) ? 0xFF : ALPHA(ce.color.first));
          else
            fprintf(out,
                    "{\\c&%06lX&\\alpha&%02X&\\frz%.4g\\frx%.8g",
                    COLOR(ce.color.first),
                    (ALPHA(ce.color.first) > 0xF8) ? 0xFF : ALPHA(ce.color.first),
                    (g_output_pos[length - 1] - ce.position.first.x) * handle.move_x,
                    ce.position.first.y * handle.move_y);
        else
          fprintf(out,
                  "{\\c&%06lX&\\alpha&%02X&",
                  COLOR(ce.color.first),
                  (ALPHA(ce.color.first) > 0xF8) ? 0xFF : ALPHA(ce.color.first));
      }

      for (unsigned int e = 0; el->character[c][e].free != 0; ce = el->character[c][++e])
        k += write_effect(out, handle, ce, &p, length);
      for (int j = 0; j < k; ++j)
        fprintf(out, ")");
      fprintf(out, "}%c", text[c]);
    }
    else
      fprintf(out, " ");
  }
  fprintf(out, "\n");

  int update = 0;
  float logo_time[50] =
    {
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1
    };
  int logo_start[50] =
    {
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    };
  int logo_stop[50] =
    {
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    };
  unsigned int t_logo = 0;
  int y = 0;
  while (l < i_time
         && ass.time[l].stop <= stop)
  {
    if (ass.time[l].length == length
        && ass.time[l].line == line + 1)
    {
      logo_start[t_logo] = ass.time[l].start;
      logo_stop[t_logo] = ass.time[l].stop;
      logo_time[t_logo] = max_angle - (49 - ass.time[l].length + ass.time[l].characters + 2 * ass.time[l].hit) * angle;
      if (y == 0)
        y = ass.time[l].line * handle.height_line + handle.margin_logo;
      ++t_logo;
    }
    else if (update == 0)
      update = l;
    ++l;
  }
  if (update != 0)
    l = update;

  if (t_logo > 0)
  {
    int p = 0;
    fprintf(out,
            "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,L,,0,0,%i,,{\\org(320,100000)\\frz%.8g",
            gethour(logo_start[0], fps),
            getmin(logo_start[0], fps),
            getsec(logo_start[0], fps),
            getsh(logo_start[0], fps),
            gethour(logo_stop[t_logo - 1], fps),
            getmin(logo_stop[t_logo - 1], fps),
            getsec(logo_stop[t_logo - 1], fps),
            getsh(logo_stop[t_logo - 1], fps),
            y,
            logo_time[0]);
    int start = logo_start[0];
    for (unsigned int i = 1; i < t_logo; ++i)
      if (logo_start[i] != logo_stop[i - 1])
      {
        fprintf(out,
                "\\t(%u,%u,\\alpha&FF&\\t(%u,%u,\\alpha&00&\\frz%.8g",
                getms(logo_stop[i - 1] - start, fps),
                getms(logo_stop[i - 1] - start, fps),
                getms(logo_start[i] - start, fps),
                getms(logo_start[i] - start, fps),
                logo_time[i]);
        p += 2;
      }
      else
      {
        fprintf(out,
                "\\t(%u,%u,\\frz%.8g",
                getms(logo_start[i] - start, fps),
                getms(logo_start[i] - start, fps),
                logo_time[i]);
        ++p;
      }
    for (int i = 0; i < p; ++i)
      fprintf(out, ")");
    fprintf(out,
            "\\p1}m 0 0 s 29 0 29 29 0 29 c{\\p0\\1c&FFFF11&\\p1}m -22 6 b -26 5 -27 0 -24 -4 -20 -10 -14 -9 -12 -8 -10 -7 -12 -6 -14 -4 -16 -2 -16 0 -16 1 -16 3 -18 7 -22 6{\\p0\\p1}m -28 -23.8 s -27 -23.8 -27 -22.8 -28 -22.8 c{\\p0\\p1}m -29.5 -19 b -29.5 -21 -28 -21 -25 -19 -28 -19 -29.5 -18 -29.5 -19{\\p0\\1c&81DAF5&\\p1}m -36 2 b -35 -2 -33 -3 -31 -3 -29 -3 -25 -1 -27 5 -28 8 -31 11 -34 12 -34 12 -33 10 -33 10 -35 13 -40 12 -42 11 -43 10 -44 11 -40 9 -38 8 -37 6 -36 2{\\p0\\p1}m -65 -1 b -63 2 -59 1 -58 -2 -57 -5 -59 -1 -61 -1 -62 -1 -62 -1 -63 -1 -62 0 -60 0 -65 -1{\\p0}\n");
  }

  free_extended_effect(el);
  free_2(text, record);
  if (ret == 0)
    return index;
  return (ret - 1);
}

static inline int
credit(t_lyrics lyrics)
{
  return (lyrics.character[0][0].size.flag == 3);
}

static void
write_lyrics(FILE* out,
             t_convert_ass ass,
             t_handle handle)
{
  unsigned int i_lyrics = ass.i_lyrics;
  unsigned int i_time = ass.i_time;
  float fps = handle.fps;
  float angle = handle.angle_logo;
  float max_angle = handle.max_angle;

  for (unsigned int i = 0, l = 0; i < i_lyrics; ++i)
    if (credit(ass.lyrics[i]))
    {
      write_credit(out, ass.lyrics[i], handle);
      continue;
    }

    else if (slider(ass.lyrics[i]))
    {
      l = write_slider(out, ass.lyrics[i], handle, ass.time, l, i_time);
      continue;
    }

    else if (several_effect(ass.lyrics[i]))
    {
      i = write_several_effect(out, ass, i, i_lyrics, handle, l, i_time);
      continue;
    }

    else if (ass.lyrics[i].character[0][0].free != 4
             && ass.lyrics[i].character[0][0].free != 5)
    {
      t_previous_data p =
        {
          .start = ass.lyrics[i].start,
          .color = ass.lyrics[i].character[0][0].color.first,
          .x_pos = ass.lyrics[i].character[0][0].position.first.x,
          .y_pos = ass.lyrics[i].character[0][0].position.first.y,
          .pos = ass.lyrics[i].character[0][0].position.flag
        };

      unsigned int length = ass.lyrics[i].length;
      unsigned int start = ass.lyrics[i].start;
      unsigned int stop = ass.lyrics[i].stop;
      unsigned int s = 1;

      for (unsigned int c = 1; c < length; ++c)
      {
        int no_change = 1;
        if (ass.lyrics[i].text[c] == ' ')
        {
          ++s;
          continue;
        }
        for (unsigned int j = 0; j < MAX_EFFECT && no_change == 1; ++j)
        {
          if (ass.lyrics[i].character[c][j].free == 0)
            if (ass.lyrics[i].character[c - s][j].free == 0)
              break;
            else
            {
              no_change = 0;
              break;
            }
          else if (ass.lyrics[i].character[c - s][j].free == 0)
          {
            no_change = 0;
            break;        
          }
          if (ass.lyrics[i].character[c][j].free != ass.lyrics[i].character[c - s][j].free
              || ass.lyrics[i].character[c][j].start != ass.lyrics[i].character[c - s][j].start
              || ass.lyrics[i].character[c][j].stop != ass.lyrics[i].character[c - s][j].stop
              || ass.lyrics[i].character[c][j].color.flag != ass.lyrics[i].character[c - s][j].color.flag
              || ass.lyrics[i].character[c][j].color.first != ass.lyrics[i].character[c - s][j].color.first
              || ass.lyrics[i].character[c][j].color.last != ass.lyrics[i].character[c - s][j].color.last
              || ass.lyrics[i].character[c][j].position.flag != ass.lyrics[i].character[c - s][j].position.flag
              || ass.lyrics[i].character[c][j].position.first.x != ass.lyrics[i].character[c - s][j].position.first.x
              || ass.lyrics[i].character[c][j].position.first.y != ass.lyrics[i].character[c - s][j].position.first.y
              || ass.lyrics[i].character[c][j].position.last.x != ass.lyrics[i].character[c - s][j].position.last.x
              || ass.lyrics[i].character[c][j].position.last.y != ass.lyrics[i].character[c - s][j].position.last.y
              || ass.lyrics[i].character[c][j].size.flag != ass.lyrics[i].character[c - s][j].size.flag
              || ass.lyrics[i].character[c][j].size.first != ass.lyrics[i].character[c - s][j].size.first
              || ass.lyrics[i].character[c][j].size.last != ass.lyrics[i].character[c - s][j].size.last)
            no_change = 0;
        }
        if (no_change == 1)
        {
          ass.lyrics[i].character[c][0].free = 0;
          ++s;
        }
        else
          s = 1;
      }

      s = 0;
      for (unsigned int c = 0; c < length; ++c)
        for (unsigned int j = 0; j < MAX_EFFECT && ass.lyrics[i].character[c][j].free != 0 && s == 0; ++j)
          if (ass.lyrics[i].character[c][j].color.flag != 1
              || (ass.lyrics[i].character[c][j].color.flag == 1
              && ALPHA(ass.lyrics[i].character[c][j].color.first) != 0xFF))
              s = 1;

      if (s == 0)
        continue;

      int position = ass.lyrics[i].character[0][0].position.flag;
      if (position == 1 || position > 8)
        fprintf(out,
                "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,%i,,{\\org(320,10000)\\c&%06lX&\\alpha&%02X&\\frz%.4g\\frx%.8g",
                gethour(start, fps),
                getmin(start, fps),
                getsec(start, fps),
                getsh(start, fps),
                gethour(stop, fps),
                getmin(stop, fps),
                getsec(stop, fps),
                getsh(stop, fps),
                (int) (ass.lyrics[i].line * handle.height_line + handle.margin_sub),
                COLOR(p.color),
                ALPHA(p.color),
                (g_output_pos[length - 1] - p.x_pos) * handle.move_x,
                p.y_pos * handle.move_y);
      else if (position == 8)
        fprintf(out,
                "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,%i,,{\\org(320,10000)\\c&%06lX&\\alpha&%02X&\\frz0\\frx0",
                gethour(start, fps),
                getmin(start, fps),
                getsec(start, fps),
                getsh(start, fps),
                gethour(stop, fps),
                getmin(stop, fps),
                getsec(stop, fps),
                getsh(stop, fps),
                (int) (ass.lyrics[i].line * handle.height_line + handle.margin_sub),
                COLOR(p.color),
                ALPHA(p.color));
      else
        fprintf(out,
                "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,D,,0,0,%i,,{\\c&%06lX&\\alpha&%02X&",
                gethour(start, fps),
                getmin(start, fps),
                getsec(start, fps),
                getsh(start, fps),
                gethour(stop, fps),
                getmin(stop, fps),
                getsec(stop, fps),
                getsh(stop, fps),
                (int) (ass.lyrics[i].line * handle.height_line + handle.margin_sub),
                COLOR(p.color),
                ALPHA(p.color));



      for (unsigned int c = 0; c < length; ++c)
      {
        if (ass.lyrics[i].text[c] != ' ')
        {
          if (ass.lyrics[i].character[c][0].free != 0)
          {
            int k = 0;
            t_character_effect ce = ass.lyrics[i].character[c][0];

            if (c != 0)
            {
              if (position == 1 || position > 8)
                fprintf(out,
                        "{\\c&%06lX&\\alpha&%02X&\\frz%.4g\\frx%.8g",
                        COLOR(ce.color.first),
                        ALPHA(ce.color.first),
                        (g_output_pos[length - 1] - ce.position.first.x) * handle.move_x,
                        ce.position.first.y * handle.move_y);
              else if (position == 8)
                fprintf(out,
                        "{\\c&%06lX&\\alpha&%02X&\\frz0\\frx0",
                        COLOR(ce.color.first),
                        ALPHA(ce.color.first));
              else
                fprintf(out,
                        "{\\c&%06lX&\\alpha&%02X&",
                        COLOR(ce.color.first),
                        ALPHA(ce.color.first));
            }

            for (unsigned int e = 0; ass.lyrics[i].character[c][e].free != 0; ce = ass.lyrics[i].character[c][++e])
              k += write_effect(out, handle, ce, &p, length);
            for (int j = 0; j < k; ++j)
              fprintf(out, ")");
            fprintf(out, "}%c", ass.lyrics[i].text[c]);
          }
          else
            fprintf(out, "%c", ass.lyrics[i].text[c]);
        }
        else
          fprintf(out, " ");
      }
      fprintf(out, "\n");

      int update = 0;
      float logo_time[50] =
        {
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1, 1, 1
        };
      int logo_start[50] =
        {
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
        };
      int logo_stop[50] =
        {
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
        };
      unsigned int t_logo = 0;
      int y = 0;
      while (l < i_time
             && ass.time[l].stop <= ass.lyrics[i].stop)
      {
        if (ass.time[l].length == ass.lyrics[i].length
            && ass.time[l].line == ass.lyrics[i].line + 1)
        {
          logo_start[t_logo] = ass.time[l].start;
          logo_stop[t_logo] = ass.time[l].stop;
          logo_time[t_logo] = max_angle - (49 - ass.time[l].length + ass.time[l].characters + 2 * ass.time[l].hit) * angle;
          if (y == 0)
            y = ass.time[l].line * handle.height_line + handle.margin_logo;
          ++t_logo;
        }
        else if (update == 0)
          update = l;
        ++l;
      }
      if (update != 0)
        l = update;

      if (t_logo > 0)
      {
        int p = 0;
        fprintf(out,
                "Dialogue: 0,%01u:%02u:%02u.%02u,%01u:%02u:%02u.%03u,L,,0,0,%i,,{\\org(320,100000)\\frz%.8g",
                gethour(logo_start[0], fps),
                getmin(logo_start[0], fps),
                getsec(logo_start[0], fps),
                getsh(logo_start[0], fps),
                gethour(logo_stop[t_logo - 1], fps),
                getmin(logo_stop[t_logo - 1], fps),
                getsec(logo_stop[t_logo - 1], fps),
                getsh(logo_stop[t_logo - 1], fps),
                y,
                logo_time[0]);
        int start = logo_start[0];
        for (unsigned int i = 1; i < t_logo; ++i)
          if (logo_start[i] != logo_stop[i - 1])
          {
            fprintf(out,
                    "\\t(%u,%u,\\alpha&FF&\\t(%u,%u,\\alpha&00&\\frz%.8g",
                    getms(logo_stop[i - 1] - start, fps),
                    getms(logo_stop[i - 1] - start, fps),
                    getms(logo_start[i] - start, fps),
                    getms(logo_start[i] - start, fps),
                    logo_time[i]);
            p += 2;
          }
          else
          {
            fprintf(out,
                    "\\t(%u,%u,\\frz%.8g",
                    getms(logo_start[i] - start, fps),
                    getms(logo_start[i] - start, fps),
                    logo_time[i]);
            ++p;
          }
        for (int i = 0; i < p; ++i)
          fprintf(out, ")");
        fprintf(out,
                "\\p1}m 0 0 s 29 0 29 29 0 29 c{\\p0\\1c&FFFF11&\\p1}m -22 6 b -26 5 -27 0 -24 -4 -20 -10 -14 -9 -12 -8 -10 -7 -12 -6 -14 -4 -16 -2 -16 0 -16 1 -16 3 -18 7 -22 6{\\p0\\p1}m -28 -23.8 s -27 -23.8 -27 -22.8 -28 -22.8 c{\\p0\\p1}m -29.5 -19 b -29.5 -21 -28 -21 -25 -19 -28 -19 -29.5 -18 -29.5 -19{\\p0\\1c&81DAF5&\\p1}m -36 2 b -35 -2 -33 -3 -31 -3 -29 -3 -25 -1 -27 5 -28 8 -31 11 -34 12 -34 12 -33 10 -33 10 -35 13 -40 12 -42 11 -43 10 -44 11 -40 9 -38 8 -37 6 -36 2{\\p0\\p1}m -65 -1 b -63 2 -59 1 -58 -2 -57 -5 -59 -1 -61 -1 -62 -1 -62 -1 -63 -1 -62 0 -60 0 -65 -1{\\p0}\n");
      }
    }
}

void
write_file(const char* file, t_convert_ass ass, t_handle handle, t_name_handle name)
{
  FILE* output = get_output(file, handle, name);

  if (output == NULL)
    return;

  write_lyrics(output, ass, handle);
  fclose(output);

  return;
}
