#ifndef FUNCTION_H_
# define FUNCTION_H_

unsigned int
gethour(int f, float fps);
unsigned int
getmin(int f, float fps);
unsigned int
getsec(int f, float fps);
unsigned int
getsh(int f, float fps);
unsigned int
getms(int f, float fps);

#endif /* !FUNCTION_H_ */
