#include "function.h"

inline unsigned int
gethour(int f, float fps)
{
  return (int) ((float) (f) / (fps * 3600.));
}

inline unsigned int
getmin(int f, float fps)
{
  return (int) ((float) (f) / (fps * 60.)) % 60;
}

inline unsigned int
getsec(int f, float fps)
{
  return (int) ((float) (f) / fps) % 60;
}

inline unsigned int
getsh(int f, float fps)
{
  return (int) ((float) (f) * 100. / fps) % 100;
}

inline unsigned int
getms(int f, float fps)
{
  return (int) ((float) (f) * 1000. / fps);
}
