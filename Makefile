CC := gcc
CFLAGS := -std=c11 -Wall -Wextra -Werror -pedantic -O3 -g -ggdb3

BINARY = convert2ass
SRCFILES := $(shell find -name '*.c')
OBJFILES := $(patsubst %.c,%.o,$(SRCFILES))

DIRS = src
BUILDDIRS = $(DIRS:%=build-%)
CLEANDIRS = $(DIRS:%=clean-%)

all: $(BINARY)

$(BINARY): $(BUILDDIRS)

$(BUILDDIRS): $(OBJFILES)
	$(MAKE) -C $(@:build-%=%)

clean: $(CLEANDIRS)

distclean: clean
	-rm -f $(BINARY)
	-rm -f test/lyrics/*.ass test/videos/*.ass test/*.ass *.ass

check: all
	./check.sh

check-play: all
	./check.sh play

$(CLEANDIRS):
	$(MAKE) -C $(@:clean-%=%) clean

.PHONY: subdirs $(BUILDDIRS)
.PHONY: subdirs $(CLEANDIRS)
.PHONY: all clean test check check-play
